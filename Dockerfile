FROM thomasgrunberg/maven-java:21

EXPOSE 30456

COPY target/competitionasaservice.jar .

CMD ["java", "-jar", "competitionasaservice.jar"]
