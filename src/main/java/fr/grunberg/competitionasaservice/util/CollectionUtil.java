package fr.grunberg.competitionasaservice.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.random.RandomGenerator;
import java.util.stream.Collectors;

public final class CollectionUtil {
	private CollectionUtil() {}
	
	public static <T> Collection<T> shuffle(Collection<T> collection, RandomGenerator randomGenerator) {
		List<T> list = collection.stream().collect(Collectors.toList());
		Collections.shuffle(list, randomGenerator);
		return list;
	}
	
	public static <T> Collection<Collection<T>> split(Collection<T> collection, int numberOfParts) {
		List<Collection<T>> splitCollection = new ArrayList<>(numberOfParts);
		for(int i = 0; i < numberOfParts; i++) {
			splitCollection.add(new ArrayList<>());
		}
		int index = 0;
		Iterator<T> iterator = collection.iterator();
		while(iterator.hasNext()) {
			splitCollection.get(index).add(iterator.next());
			index++;
			if(index >= splitCollection.size())
				index = 0;
		}
		return splitCollection;
	}
}