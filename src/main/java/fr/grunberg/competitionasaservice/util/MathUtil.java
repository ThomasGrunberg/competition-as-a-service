package fr.grunberg.competitionasaservice.util;

public final class MathUtil {
	private MathUtil() {}
	
	public static boolean isPowerOfTwo(int number) {
		double logDiv = Math.log(number) / Math.log(2);
		return (int)(Math.ceil(logDiv)) == (int)(Math.floor(logDiv));
	}
	public static int getNearestInferiorOrEqualPowerOfTwo(int number) {
		if(number == 0)
			return 0;
		return 1 << (31 - Integer.numberOfLeadingZeros(number));
	}
}