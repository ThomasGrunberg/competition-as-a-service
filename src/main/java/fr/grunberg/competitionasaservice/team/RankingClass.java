package fr.grunberg.competitionasaservice.team;

public enum RankingClass {
	FAVORITE,
	OUTSIDER,
	AVERAGE,
	UNEXPECTED,
	BOTTOM
}
