package fr.grunberg.competitionasaservice.team;

public record Ranking(int position, RankingClass rankingClass) {}