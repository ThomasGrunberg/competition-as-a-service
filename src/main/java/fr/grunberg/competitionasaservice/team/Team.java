package fr.grunberg.competitionasaservice.team;

import java.util.Collection;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.TeamDto;
import fr.grunberg.competitionasaservice.dto.Transferable;
import fr.grunberg.competitionasaservice.geography.Country;

public record Team (int id, String name, Country country, Collection<Player> players)
	implements Transferable<TeamDto>, Comparable<Team> {

	@Override
	public TeamDto toDto(DataController dataController) {
		Ranking ranking = dataController.getRanking(this);
		return new TeamDto(id, name, 
				getDefense(),
				getAttack(),
				ranking.position(),
				ranking.rankingClass().name(),
				country.continent().name(),
				country.continent().world().name(),
				players.stream().map(p -> p.toDto(dataController)).toList()
				);
	}
	public int getDefense() {
		return players.stream().mapToInt(p -> p.defense()).sum();
	}
	public int getAttack() {
		return players.stream().mapToInt(p -> p.attack()).sum();
	}
	@Override
	public String toString() {
		return name + " (" + getDefense() + "/" + getAttack();
	}
	@Override
	public int compareTo(Team o) {
		if(o == null)
			return -1;
		if(o.getDefense() + o.getAttack() > getDefense() + getAttack())
			return 1;
		if(o.getDefense() + o.getAttack() < getDefense() + getAttack())
			return -1;
		if(o.getAttack() > getAttack())
			return 1;
		if(o.getAttack() < getAttack())
			return -1;
		return 0;
	}
}
