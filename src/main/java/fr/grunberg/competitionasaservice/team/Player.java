package fr.grunberg.competitionasaservice.team;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.PlayerDto;
import fr.grunberg.competitionasaservice.dto.Transferable;

public record Player(int id, int defense, int attack)
	implements Transferable<PlayerDto> {

	@Override
	public PlayerDto toDto(DataController dataController) {
		return new PlayerDto(id, defense, attack);
	}
}