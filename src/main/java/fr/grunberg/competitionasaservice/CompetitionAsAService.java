package fr.grunberg.competitionasaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CompetitionAsAService extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(CompetitionAsAService.class, args);
	}
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CompetitionAsAService.class);
    }
}
