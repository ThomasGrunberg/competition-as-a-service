package fr.grunberg.competitionasaservice;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Component
public class EndpointsListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	System.out.println("List of endpoints :");
        ApplicationContext applicationContext = event.getApplicationContext();
        applicationContext
        	.getBean(RequestMappingHandlerMapping.class)
        	.getHandlerMethods()
        	.forEach(this::print);
        System.out.println("End of endpoints list.");
    }
    
    private void print(RequestMappingInfo r, HandlerMethod h) {
    	System.out.println(r.toString() + " " + h.toString());
    }
}
