package fr.grunberg.competitionasaservice.competition;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.PoolDto;
import fr.grunberg.competitionasaservice.dto.Transferable;
import fr.grunberg.competitionasaservice.team.Team;
import fr.grunberg.competitionasaservice.util.Listener;

public class Pool implements Transferable<PoolDto>, Listener {
	private final int id;
	private final String name;
	private final Collection<Game> games;
	private final Collection<Listener> listeners;
	private final Collection<TeamSpot> teams;
	private final Map<Integer, Pool> selectedForNextPool;
	private Competition competition;

	private Map<Team, PoolStatistics> statistics;
	
	public Pool(int id, String name, Collection<Game> games) {
		this.id = id;
		this.name = name;
		this.games = games;
		this.listeners = new HashSet<>();
		this.selectedForNextPool = new HashMap<>();
		this.games.stream()
				.filter(g -> g.getHomeTeam().getOriginPool() != null)
				.forEach(g -> g.getHomeTeam().getOriginPool().addListener(this));
		this.games.stream()
				.filter(g -> g.getAwayTeam().getOriginPool() != null)
				.forEach(g -> g.getAwayTeam().getOriginPool().addListener(this));
		games.forEach(g -> g.setPool(this));
		teams = games.stream()
				.map(g -> List.of(g.getHomeTeam(), g.getAwayTeam()))
				.flatMap(t -> t.stream())
				.distinct()
				.toList();
		teams.stream()
				.filter(t -> t.getOriginPool() != null)
				.forEach(team -> team.getOriginPool().select(this, team.getOriginPoolPosition()));
		recalculateStatistics();
	}

	public String getName() {
		return name;
	}
	public final Collection<Game> getGames() {
		return games;
	}
	public final Collection<TeamSpot> getTeamSpots() {
		return games.stream()
			.map(g -> List.of(g.getHomeTeam(), g.getAwayTeam()))
			.flatMap(t -> t.stream())
			.distinct()
			.collect(Collectors.toSet());
	}

	public final boolean isFinished() {
		return games.stream().allMatch(Game::isFinished);
	}
	
	public final void recalculateStatistics() {
		statistics = getTeamSpots().stream()
				.filter(team -> team.isDecided())
				.collect(Collectors.toMap(team -> team.getTeam().get(), team -> recalculateStatistics(team)));
		if(isFinished()) {
			listeners.stream().forEach(l -> l.check());
		}
	}
	private final PoolStatistics recalculateStatistics(TeamSpot team) {
		if(team.isDecided()) {
			Collection<Game> teamGames = games.stream()
					.filter(g -> g.getHomeTeam().equals(team) || g.getAwayTeam().equals(team))
					.toList();
			return new PoolStatistics(this, team, 
					teamGames.stream().filter(g -> g.isFinished()).mapToInt(g -> PoolStatistics.getPoints(g.getResult(team))).sum(),
					teamGames.stream().filter(g -> g.isFinished() && g.getHomeTeam().equals(team)).mapToInt(g -> g.getHomeGoals()).sum()
						+ teamGames.stream().filter(g -> g.isFinished() && g.getAwayTeam().equals(team)).mapToInt(g -> g.getAwayGoals()).sum(),
					teamGames.stream().filter(g -> g.isFinished() && g.getHomeTeam().equals(team)).mapToInt(g -> g.getAwayGoals()).sum()
						+ teamGames.stream().filter(g -> g.isFinished() && g.getAwayTeam().equals(team)).mapToInt(g -> g.getHomeGoals()).sum(),
					(int) teamGames.stream().filter(g -> g.isFinished() && g.getResult(team) == TeamGameResult.VICTORY).count(),
					(int) teamGames.stream().filter(g -> g.isFinished() && g.getResult(team) == TeamGameResult.DRAW).count(),
					(int) teamGames.stream().filter(g -> g.isFinished() && g.getResult(team) == TeamGameResult.LOSS).count(),
					(int) teamGames.stream().filter(g -> !g.isFinished()).count()
					);
		}
		else
			return new PoolStatistics(this, team, 0, 0, 0, 0, 0, 0, 0);
	}

	public final Optional<Team> getTeamAtPosition(int position) {
		return statistics.values().stream()
				.sorted()
				.map(s -> s.team().getTeam())
				.toList()
				.get(position-1);
	}
	public final PoolStatistics getTeamStatistics(Team team) {
		return statistics.get(team);
	}
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public PoolDto toDto(DataController dataController) {
		return new PoolDto(id, name, 
				games.stream().map(g -> g.toDto(dataController)).toList(), 
				teams.stream().map(t -> t.toDto(dataController)).toList(), 
				statistics.values().stream().sorted().map(s -> s.toDto(dataController)).toList(),
				isFinished()
				);
	}

	public int getId() {
		return id;
	}

	public Competition getCompetition() {
		return competition;
	}

	public final Collection<TeamSpot> getTeams() {
		return teams;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public void addListener(Listener listener) {
		this.listeners.add(listener);
	}
	public void select(Pool nextPool, int position) {
		if(selectedForNextPool.containsKey(position)) {
			throw new IllegalArgumentException("Position " + position + " cannot be selected for " + nextPool + ", it is already selected for " + selectedForNextPool.get(position));
		}
		selectedForNextPool.put(position, nextPool);
		this.listeners.add(nextPool);
	}

	@Override
	public void check() {
		games.stream().forEach(g -> g.getHomeTeam().checkOriginPool());
		games.stream().forEach(g -> g.getAwayTeam().checkOriginPool());
		recalculateStatistics();
	}

	public boolean isSelected(TeamSpot team) {
		if(selectedForNextPool.isEmpty())
			return false;
		if(statistics.isEmpty())
			return false;
		int rank = getRank(team);
		return selectedForNextPool.containsKey(rank);
	}

	private int getRank(TeamSpot team) {
		List<PoolStatistics> rankedTeams = statistics.values().stream().sorted().toList();
		for(int rank = 1; rank <= rankedTeams.size(); rank++) {
			if(rankedTeams.get(rank-1).team().equals(team))
				return rank;
		}
		return 0;
	}
}
