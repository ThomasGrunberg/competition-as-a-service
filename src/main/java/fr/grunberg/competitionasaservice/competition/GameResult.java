package fr.grunberg.competitionasaservice.competition;

public enum GameResult {
	HOME_VICTORY,
	AWAY_VICTORY,
	DRAW,
	UNPLAYED
}
