package fr.grunberg.competitionasaservice.competition;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.PoolStatisticsDto;
import fr.grunberg.competitionasaservice.dto.Transferable;

public record PoolStatistics (
		Pool pool,
		TeamSpot team,
		int points,
		int goalsScored,
		int goalsReceived,
		int victories,
		int defeats,
		int draws,
		int unplayed
		)  implements Comparable<PoolStatistics>, Transferable<PoolStatisticsDto>{

	@Override
	public int compareTo(PoolStatistics other) {
		if(other.points() > points)
			return 1;
		if(other.points() < points)
			return -1;
		if(other.goalsScored() - other.goalsReceived() > goalsScored - goalsReceived)
			return 1;
		if(other.goalsScored() - other.goalsReceived() < goalsScored - goalsReceived)
			return -1;
		if(other.goalsScored() > goalsScored)
			return 1;
		if(other.goalsScored() < goalsScored)
			return -1;
		return 0;
	}
	
	public static int getPoints(TeamGameResult result) {
		return switch(result) {
			case VICTORY -> {yield 3;}
			case DRAW -> {yield 1;}
			default -> {yield 0;}
		};
	}

	@Override
	public PoolStatisticsDto toDto(DataController dataController) {
		return new PoolStatisticsDto (
				team.toDto(dataController),
				points,
				goalsScored,
				goalsReceived,
				victories,
				defeats,
				draws,
				unplayed,
				pool.isSelected(team)
				);
	}
}
