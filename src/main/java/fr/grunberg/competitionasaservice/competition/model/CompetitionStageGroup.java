package fr.grunberg.competitionasaservice.competition.model;

public enum CompetitionStageGroup {
	WORLD,
	CONTINENT
}
