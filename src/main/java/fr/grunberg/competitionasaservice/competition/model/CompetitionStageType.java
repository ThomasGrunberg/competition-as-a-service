package fr.grunberg.competitionasaservice.competition.model;

public enum CompetitionStageType {
	CHAMPIONSHIP,
	POOL,
	ELIMINATION
}
