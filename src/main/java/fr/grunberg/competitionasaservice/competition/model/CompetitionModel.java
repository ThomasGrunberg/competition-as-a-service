package fr.grunberg.competitionasaservice.competition.model;

import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;
import java.util.stream.Collectors;

import fr.grunberg.competitionasaservice.competition.Competition;
import fr.grunberg.competitionasaservice.competition.Game;
import fr.grunberg.competitionasaservice.competition.Pool;
import fr.grunberg.competitionasaservice.competition.SingleGamePool;
import fr.grunberg.competitionasaservice.competition.TeamSpot;
import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.CompetitionModelDto;
import fr.grunberg.competitionasaservice.dto.Transferable;
import fr.grunberg.competitionasaservice.geography.Continent;
import fr.grunberg.competitionasaservice.geography.Location;
import fr.grunberg.competitionasaservice.geography.World;
import fr.grunberg.competitionasaservice.team.Team;
import fr.grunberg.competitionasaservice.util.CollectionUtil;
import fr.grunberg.competitionasaservice.util.MathUtil;

public final class CompetitionModel implements Transferable<CompetitionModelDto> {
	private final int id;
	private final String name;
	private final SequencedCollection<CompetitionStageModel> stages;
	
	private final int gameIntervals = 2;
	private DataController dataController;
	
	public CompetitionModel(int id, String name, SequencedCollection<CompetitionStageModel> stages) {
		this.id = id;
		this.name = name;
		this.stages = stages;
	}
	
	public Competition instantiate(DataController dataController, ChronoLocalDate startDate) {
		this.dataController = dataController;
		if(stages.size() == 1) {
			CompetitionStageModel stage = stages.getFirst();
			if(stage.type() == CompetitionStageType.CHAMPIONSHIP 
					|| stage.type() == CompetitionStageType.ELIMINATION
					) {
				List<Team> teams;
				if(stage.location() instanceof World world) {
					teams = dataController.getTeams().stream()
							.filter(t -> t.country().continent().world().equals(world))
							.collect(Collectors.toList());
				}
				else if(stage.location() instanceof Continent continent) {
					teams = dataController.getTeams().stream()
							.filter(t -> t.country().continent().equals(continent))
							.collect(Collectors.toList());
				}
				else {
					teams = dataController.getTeams().stream().collect(Collectors.toList());
				}
				Collections.shuffle(teams, dataController.getRandomGenerator());
				if(stage.type() == CompetitionStageType.CHAMPIONSHIP) {
					Competition newCompetition = new Competition(dataController.getNextId(),
						name + " " + startDate.get(ChronoField.YEAR),
						startDate, this,
						List.of(
								createChampionshipPool(teams.stream().map(t -> new TeamSpot(dataController.getNextId(), t, startDate)).toList(),
										startDate)
								)
						);
					dataController.add(newCompetition);
					return newCompetition;
				}
				else if(stage.type() == CompetitionStageType.ELIMINATION) {
					Collection<Pool> pools = createEliminationPools(teams.stream()
							.map(t -> new TeamSpot(dataController.getNextId(), t, startDate))
							.toList(), startDate);
					
					Competition newCompetition = new Competition(dataController.getNextId(), 
							name + " " + startDate.get(ChronoField.YEAR),
							startDate, this,
							pools
							);
					dataController.add(newCompetition);
					return newCompetition;
				}

			}
		}
		else {
			Iterator<CompetitionStageModel> iStages = stages.iterator();
			CompetitionStageModel firstStage = iStages.next();
			Collection<Team> teamsForFirstStage;
			SequencedCollection<Pool> pools = new ArrayList<>();
			if(firstStage.location() instanceof Continent continent) {
				teamsForFirstStage = dataController.getTeams().stream().filter(t -> t.country().continent().equals(continent)).toList();
			}
			else if(firstStage.location() instanceof World world) {
				teamsForFirstStage = dataController.getTeams().stream().filter(t -> t.country().continent().world().equals(world)).toList();
			}
			else {
				teamsForFirstStage = dataController.getTeams();
			}

			if(firstStage.group() == CompetitionStageGroup.CONTINENT) {
				Collection<Continent> continents = teamsForFirstStage.stream().map(t -> t.country().continent()).collect(Collectors.toSet());
				for(Continent continent : continents) {
					pools.addAll(createSplitPools( 
							teamsToTeamSpots(teamsForFirstStage.stream().filter(t -> t.country().continent().equals(continent)).toList()
									, startDate, true)
							, continent.name(), startDate));
				}
			}
			else if(firstStage.group() == CompetitionStageGroup.WORLD) {
				Collection<World> worlds = teamsForFirstStage.stream().map(t -> t.country().continent().world()).collect(Collectors.toSet());
				for(World world : worlds) {
					pools.addAll(createSplitPools(teamsToTeamSpots(teamsForFirstStage.stream().filter(t -> t.country().continent().world().equals(world)).toList()
									, startDate, true)
							, world.name(), startDate));
				}
			}
			else {
				pools.add(createChampionshipPool(teamsToTeamSpots(teamsForFirstStage, startDate, true), startDate));
			}
			
			Collection<TeamSpot> teamSpotsFromPreviousStage = getTeamSpots(pools, firstStage.selected(), startDate);
			while(iStages.hasNext()) {
				CompetitionStageModel stage = iStages.next();
				
				if(stage.type() == CompetitionStageType.ELIMINATION) {
					pools.addAll(createEliminationPools(teamSpotsFromPreviousStage, startDate));
				}
				else {
					Collection<Pool> newPools = createSplitPools(teamSpotsFromPreviousStage, name, startDate);
					teamSpotsFromPreviousStage = getTeamSpots(newPools, stage.selected(), startDate);
					pools.addAll(newPools);
				}
			}
			Competition newCompetition = new Competition(dataController.getNextId(), 
					name + " " + startDate.get(ChronoField.YEAR),
					startDate, this,
					pools
					);
			dataController.add(newCompetition);
			return newCompetition;
		}
		throw new IllegalStateException("Model cannot be instantiated.");
	}
	
	private Collection<TeamSpot> getTeamSpots(Collection<Pool> pools, int numberToSelect, ChronoLocalDate nextDate) {
		int position = 1;
		Collection<TeamSpot> teamSpots = new HashSet<>();
		Iterator<Pool> iPools = pools.iterator();
		while(teamSpots.size() < numberToSelect) {
			if(!iPools.hasNext()) {
				iPools = pools.iterator();
				position++;
			}
			Pool pool = iPools.next();
			if(pool.getTeamSpots().size() >= position)
				teamSpots.add(new TeamSpot(dataController.getNextId(), pool, position, nextDate));
		}
		return teamSpots;
	}

	private Collection<Game> createGames(Collection<TeamSpot> teams, boolean homeAndAway, ChronoLocalDate startDate) {
		ChronoLocalDate nextDate = startDate;
		Collection<Game> games = new ArrayList<>(teams.size() * (teams.size()-1) * 2);
		for(TeamSpot teamA : teams) {
			for(TeamSpot teamB : teams) {
				if(teamA != teamB && 
						(homeAndAway ||
						!games.stream().anyMatch(g -> g.withTeams(teamA, teamB))
						)) {
						games.add(new Game(dataController.getNextId(), 
								teamA, 
								teamB, 
								nextDate));
						nextDate = nextDate.plus(gameIntervals, ChronoUnit.DAYS);
				}
			}
		}
		return games;
	}
	
	private Pool createChampionshipPool(Collection<TeamSpot> teams, ChronoLocalDate startDate) {
		return new Pool(dataController.getNextId(), name,
				createGames(teams, true, startDate)
				);
	}
	
	private Pool createLocalPool(Collection<TeamSpot> teams, ChronoLocalDate startDate, String poolPrefix, int poolNumber) {
		return new Pool(dataController.getNextId(), poolPrefix + " " + String.valueOf((char) ('A' + poolNumber - 1)),
				createGames(teams, false, startDate)
				);
	}
	
	private Collection<TeamSpot> teamsToTeamSpots(Collection<Team> teams, ChronoLocalDate startDate, boolean shuffle) {
		Collection<TeamSpot> teamSpots = teams.stream().map(t -> new TeamSpot(dataController.getNextId(), t, startDate)).collect(Collectors.toList());
		if(shuffle)
			CollectionUtil.shuffle(teamSpots, dataController.getRandomGenerator());
		return teamSpots;
	}
	
	static Collection<Integer> spreadSelectedSpots(
			Collection<? extends Collection<? extends Object>> teamsInPools, int spotsAvailable) {
		if(spotsAvailable < teamsInPools.size())
			throw new IllegalArgumentException("Cannot split the slots.");
		else if(spotsAvailable % teamsInPools.size() == 0) {
			return teamsInPools.stream()
					.map(t -> (int) (spotsAvailable / teamsInPools.size()))
					.toList();
		}
		else {
			List<Integer> selected = new ArrayList<>(teamsInPools.size());
			for(int i = 0; i < teamsInPools.size(); i++) {
				selected.add(1);
			}
			while(selected.stream().mapToInt(Integer::intValue).sum() < spotsAvailable) {
				for(int i = 0; i < teamsInPools.size()
						&& selected.stream().mapToInt(Integer::intValue).sum() < spotsAvailable
						; i++) {
					selected.set(i, selected.get(i)+1);
				}
			}
			return selected;
		}
	}
	
	private SequencedCollection<TeamSpot> sortTeamSpots(Collection<TeamSpot> teamSpots) {
		SequencedCollection<TeamSpot> sortedTeamSpots = new ArrayList<>(teamSpots.size());
		Iterator<TeamSpot> bestTeams = teamSpots.stream()
				.sorted((t1, t2) -> {
					if(t1.getOriginPoolPosition() - t2.getOriginPoolPosition() > 0)
						return 1;
					if(t1.getOriginPoolPosition() - t2.getOriginPoolPosition() < 0)
						return -1;
					return t1.getOriginPool().getId() - t2.getOriginPool().getId();
					})
				.iterator();
		Iterator<TeamSpot> worstTeams = teamSpots.stream()
				.sorted((t1, t2) -> {
					if(t1.getOriginPoolPosition() - t2.getOriginPoolPosition() > 0)
						return -1;
					if(t1.getOriginPoolPosition() - t2.getOriginPoolPosition() < 0)
						return 1;
					return t2.getOriginPool().getId() - t1.getOriginPool().getId();
					})
				.iterator();
		while(sortedTeamSpots.size() < teamSpots.size()) {
			sortedTeamSpots.add(bestTeams.next());
			sortedTeamSpots.add(worstTeams.next());
		}
		return sortedTeamSpots;
	}
	
	private Collection<Pool> createEliminationPools(Collection<TeamSpot> teamSpots, ChronoLocalDate nextDate) {
		SequencedCollection<Pool> pools = new ArrayList<>();
		boolean firstStageElimination = true;
		while(teamSpots.size() > 1) {
			if(firstStageElimination &&
					teamSpots.stream().allMatch(t -> t.getOriginPoolPosition() != null)
					) {
				teamSpots = sortTeamSpots(teamSpots);
			}
			else {
				teamSpots = CollectionUtil.shuffle(teamSpots, dataController.getRandomGenerator());
			}
			Collection<TeamSpot> teamSpotsToAssignNextRound = new ArrayList<>();
			int numberOfGamesToCreate;
			if(MathUtil.isPowerOfTwo(teamSpots.size())) {
				numberOfGamesToCreate = teamSpots.size()/2;
			}
			else {
				numberOfGamesToCreate = MathUtil.getNearestInferiorOrEqualPowerOfTwo(teamSpots.size());
			}
			String poolNameSuffix;
			if(teamSpots.size() == 2)
				poolNameSuffix = "final";
			else if(teamSpots.size() <= 4)
				poolNameSuffix = "semi-final";
			else if(teamSpots.size() <= 8)
				poolNameSuffix = "quarter-final";
			else if(teamSpots.size() <= 16)
				poolNameSuffix = "eigth-final";
			else
				poolNameSuffix = "elimination round";
			Iterator<TeamSpot> iTeamSpotsToAssign = teamSpots.iterator();
			for(int g = 0; g < numberOfGamesToCreate; g++) {
				pools.add(new SingleGamePool(dataController.getNextId(), name + " " + poolNameSuffix + 
						(teamSpots.size() > 2 ? " " + (g+1) : "")
						,
						new Game(dataController.getNextId(), iTeamSpotsToAssign.next(), iTeamSpotsToAssign.next(), nextDate)
						));
				teamSpotsToAssignNextRound.add(new TeamSpot(dataController.getNextId(), pools.getLast(), 1, nextDate));
				nextDate = nextDate.plus(gameIntervals, ChronoUnit.DAYS);
			}
			while(iTeamSpotsToAssign.hasNext())
				teamSpotsToAssignNextRound.add(iTeamSpotsToAssign.next());
			teamSpots = teamSpotsToAssignNextRound;
			firstStageElimination = false;
		}
		return pools;
	}

	private Collection<Pool> createSplitPools(Collection<TeamSpot> teamSpots, String poolPrefix, ChronoLocalDate startDate) {
		int numberOfTeams = teamSpots.size();
		if(numberOfTeams <= 5)
			return List.of(createLocalPool(teamSpots, startDate, poolPrefix, 1));
		else {
			int numberOfPools = getBestPoolSplitSize(numberOfTeams);
			Iterator<Collection<TeamSpot>> splitTeamSpots = CollectionUtil.split(teamSpots, numberOfPools).iterator();
			Collection<Pool> pools = new LinkedList<>();
			for(int i = 1; i <= numberOfPools; i++) {
				pools.add(createLocalPool(splitTeamSpots.next(), startDate, poolPrefix, i));
			}
			return pools;
		}
	}
	private int getBestPoolSplitSize(int numberOfTeams) {
		if(numberOfTeams % 5 == 0)
			return (numberOfTeams / 5);
		if(numberOfTeams % 4 == 0)
			return (numberOfTeams / 4);
		if(numberOfTeams % 6 == 0)
			return (numberOfTeams / 6);
		return numberOfTeams / 4;
	}
	
	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	@Override
	public CompetitionModelDto toDto(DataController dataController) {
		return new CompetitionModelDto(id, name);
	}

	public Location getRestriction() {
		return stages.getFirst().location();
	}
}
