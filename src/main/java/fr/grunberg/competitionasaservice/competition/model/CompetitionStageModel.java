package fr.grunberg.competitionasaservice.competition.model;

import fr.grunberg.competitionasaservice.geography.Location;

public record CompetitionStageModel (
		CompetitionStageType type,
		CompetitionStageGroup group,
		Location location,
		int selected
		) {
}
