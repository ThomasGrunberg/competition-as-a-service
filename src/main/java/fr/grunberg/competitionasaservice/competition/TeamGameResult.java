package fr.grunberg.competitionasaservice.competition;

public enum TeamGameResult {
	VICTORY,
	LOSS,
	DRAW,
	UNPLAYED
}
