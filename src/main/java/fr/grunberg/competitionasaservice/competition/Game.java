package fr.grunberg.competitionasaservice.competition;

import java.time.chrono.ChronoLocalDate;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.GameDto;
import fr.grunberg.competitionasaservice.dto.Transferable;

public final class Game implements Comparable<Game>, Transferable<GameDto> {
	private final int id;
	private final TeamSpot homeTeam;
	private final TeamSpot awayTeam;
	private final ChronoLocalDate date;
	private Pool pool;
	private boolean finished;
	private int homeGoals;
	private int awayGoals;
	
	public Game(int id, TeamSpot homeTeam, TeamSpot awayTeam, ChronoLocalDate date) {
		this.id = id;
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.date = date;
		this.finished = false;
	}
	
	public void play(DataController dataController) {
		if(finished)
			return;
		if(!homeTeam.isDecided())
			return;
		if(!awayTeam.isDecided())
			return;
		boolean allowDraw = allowDraw();
		homeGoals = 
			Math.max(0,
				(dataController.getRandomGenerator().nextInt(0, dataController.getLuckFactor())
				+ homeTeam.getTeam().get().getAttack()
				- awayTeam.getTeam().get().getDefense()
				) / 5);
		awayGoals = 
			Math.max(0,
				(dataController.getRandomGenerator().nextInt(0, dataController.getLuckFactor())
				+ awayTeam.getTeam().get().getAttack()
				- homeTeam.getTeam().get().getDefense()
				) / 5);
		if(!allowDraw && homeGoals == awayGoals) {
			homeGoals += 
				Math.max(0,
					(dataController.getRandomGenerator().nextInt(0, dataController.getLuckFactor())
					+ homeTeam.getTeam().get().getAttack()
					- awayTeam.getTeam().get().getDefense()
					) / 10);
			awayGoals += 
				Math.max(0,
					(dataController.getRandomGenerator().nextInt(0, dataController.getLuckFactor())
					+ awayTeam.getTeam().get().getAttack()
					- homeTeam.getTeam().get().getDefense()
					) / 10);
		}
		if(!allowDraw && homeGoals == awayGoals) {
			if(dataController.getRandomGenerator().nextBoolean()) {
				homeGoals++;
			}
			else {
				awayGoals++;
			}
		}
		finished = true;
	}
	private boolean allowDraw() {
		return getPool().getGames().size() > 1;
	}
	public boolean isFinished() {
		return finished;
	}
	public TeamSpot getHomeTeam() {
		return homeTeam;
	}
	public TeamSpot getAwayTeam() {
		return awayTeam;
	}
	public int getHomeGoals() {
		return homeGoals;
	}
	public int getAwayGoals() {
		return awayGoals;
	}

	public TeamGameResult getResult(TeamSpot team) {
		GameResult gameResult = getResult();
		if(gameResult == GameResult.UNPLAYED)
			return TeamGameResult.UNPLAYED;
		if(gameResult == GameResult.DRAW)
			return TeamGameResult.DRAW;
		else if(team.equals(homeTeam) && gameResult == GameResult.HOME_VICTORY)
			return TeamGameResult.VICTORY;
		else if(team.equals(homeTeam) && gameResult == GameResult.AWAY_VICTORY)
			return TeamGameResult.LOSS;
		else if(team.equals(awayTeam) && gameResult == GameResult.HOME_VICTORY)
			return TeamGameResult.LOSS;
		else
			return TeamGameResult.VICTORY;
	}
	public GameResult getResult() {
		if(!isFinished())
			return GameResult.UNPLAYED;
		if(homeGoals == awayGoals)
			return GameResult.DRAW;
		if(homeGoals > awayGoals)
			return GameResult.HOME_VICTORY;
		else
			return GameResult.AWAY_VICTORY;
	}

	public ChronoLocalDate getDate() {
		return date;
	}

	@Override
	public int compareTo(Game o) {
		if(date.isBefore(o.getDate()))
			return -1;
		else if(date.isAfter(o.getDate()))
			return 1;
		return 0;
	}
	
	@Override
	public String toString() {
		return homeTeam.toString() + " vs " + awayTeam.toString() + " (" + pool.toString() + ")";
	}

	public boolean withTeams(TeamSpot teamA, TeamSpot teamB) {
		if(teamA.equals(homeTeam) && teamB.equals(awayTeam))
			return true;
		if(teamA.equals(awayTeam) && teamB.equals(homeTeam))
			return true;
		return false;
	}

	@Override
	public GameDto toDto(DataController dataController) {
		return new GameDto(id, 
				homeTeam.toDto(dataController), 
				awayTeam.toDto(dataController),
				finished,
				getResult().name(),
				homeGoals,
				awayGoals,
				date);
	}
	public final int getId() {
		return id;
	}

	public Pool getPool() {
		return pool;
	}

	public void setPool(Pool pool) {
		this.pool = pool;
	}
}
