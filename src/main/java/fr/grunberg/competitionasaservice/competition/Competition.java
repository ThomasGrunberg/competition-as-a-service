package fr.grunberg.competitionasaservice.competition;

import java.time.chrono.ChronoLocalDate;
import java.util.Collection;

import fr.grunberg.competitionasaservice.competition.model.CompetitionModel;
import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.CompetitionDto;
import fr.grunberg.competitionasaservice.dto.Transferable;
import fr.grunberg.competitionasaservice.geography.Location;

public final class Competition implements Transferable<CompetitionDto> {
	private final int id;
	private final String name;
	private final ChronoLocalDate startDate;
	private final CompetitionModel model;
	private final Location restriction;
	private final Collection<Pool> pools;
	
	public Competition(int id, String name, ChronoLocalDate startDate, CompetitionModel model, Collection<Pool> pools) {
		this.id = id;
		this.name = name;
		this.startDate = startDate;
		this.model = model;
		this.pools = pools;
		this.restriction = model.getRestriction();
		pools.forEach(p -> p.setCompetition(this));
	}

	public String getName() {
		return name;
	}
	public Collection<Pool> getPools() {
		return pools;
	}
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public CompetitionDto toDto(DataController dataController) {
		return new CompetitionDto(id, name, (restriction != null ?restriction.name():null), pools.stream().map(p -> p.toDto(dataController)).toList());
	}

	public final int getId() {
		return id;
	}

	public ChronoLocalDate getStartDate() {
		return startDate;
	}

	public CompetitionModel getModel() {
		return model;
	}
}
