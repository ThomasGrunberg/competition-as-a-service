package fr.grunberg.competitionasaservice.competition;

import java.util.List;

public final class SingleGamePool extends Pool{
	public SingleGamePool(int id, String name, Game games) {
		super(id, name, List.of(games));
	}
}
