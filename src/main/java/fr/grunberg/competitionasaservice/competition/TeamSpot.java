package fr.grunberg.competitionasaservice.competition;

import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.TeamSpotDto;
import fr.grunberg.competitionasaservice.dto.Transferable;
import fr.grunberg.competitionasaservice.team.Team;

public final class TeamSpot implements Transferable<TeamSpotDto> {
	private final int id;
	private final Pool originPool;
	private final Integer originPoolPosition;
	private Team team;
	private ChronoLocalDate nextDate;
	
	public TeamSpot(int id, Pool originPool, int originPoolPosition, ChronoLocalDate firstDate) {
		if(originPool != null && originPool.getTeamSpots().size() < originPoolPosition)
			throw new IllegalArgumentException("Cannot assign position " + originPoolPosition + " for pool " + originPool + " : only " + originPool.getTeamSpots().size() + " teams.");
		this.id = id;
		this.originPool = originPool;
		this.originPoolPosition = originPoolPosition;
		this.nextDate = firstDate;
	}
	
	public TeamSpot(int id, Team team, ChronoLocalDate firstDate) {
		this.id = id;
		this.originPool = null;
		this.originPoolPosition = null;
		this.team = team;
		this.nextDate = firstDate;
	}
	
	public ChronoLocalDate getNextDate(int intervalDates) {
		ChronoLocalDate dateToReturn = nextDate;
		nextDate = nextDate.plus(intervalDates, ChronoUnit.DAYS);
		return dateToReturn;
	}
	
	public boolean isDecided() {
		if(team != null)
			return true;
		return originPool.isFinished();
	}
	public Optional<Team> getTeam() {
		if(team == null) {
			if(originPool.isFinished()) {
				team = originPool.getTeamAtPosition(originPoolPosition).get();
			}
		}
		return Optional.ofNullable(team);
	}
	public Pool getOriginPool() {
		return originPool;
	}

	public Integer getOriginPoolPosition() {
		return originPoolPosition;
	}

	@Override
	public String toString() {
		if(team != null)
			return team.name();
		else
			return "TBD";
	}
	@Override
	public int hashCode() {
		if(team != null)
			return team.hashCode();
		else
			return this.originPoolPosition;
	}
	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		if(o instanceof TeamSpot other) {
			if(this.team != null && other.getTeam().isPresent()) {
				return this.team.equals(other.getTeam().get());
			}
			if(originPool != null 
					&& originPool.equals(other.getOriginPool())
					&& originPoolPosition.equals(other.getOriginPoolPosition()))
				return true;
		}
		return false;
	}

	@Override
	public TeamSpotDto toDto(DataController dataController) {
		return new TeamSpotDto(id, 
				(originPool == null ? null : originPool.getId()), 
				originPoolPosition, 
				(originPool == null ? null : originPool.getName()), 
				(team == null ? null : team.toDto(dataController)), 
				nextDate);
	}
	public final int getId() {
		return id;
	}

	public void checkOriginPool() {
		if(team == null 
				&& originPool != null 
				&& originPool.isFinished()) {
			team = originPool.getTeamAtPosition(originPoolPosition).orElse(null);
		}
	}
}
