package fr.grunberg.competitionasaservice.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.random.RandomGenerator;
import java.util.stream.Collectors;

import fr.grunberg.competitionasaservice.competition.Competition;
import fr.grunberg.competitionasaservice.competition.Game;
import fr.grunberg.competitionasaservice.competition.Pool;
import fr.grunberg.competitionasaservice.competition.model.CompetitionModel;
import fr.grunberg.competitionasaservice.geography.Continent;
import fr.grunberg.competitionasaservice.geography.Country;
import fr.grunberg.competitionasaservice.geography.World;
import fr.grunberg.competitionasaservice.team.Ranking;
import fr.grunberg.competitionasaservice.team.RankingClass;
import fr.grunberg.competitionasaservice.team.Team;

public abstract class AbstractDataController implements DataController {
	private final RandomGenerator randomGenerator;

	private Map<Integer, World> worlds;
	private Map<Integer, Continent> continents;
	private Map<Integer, Country> countries;
	private Map<Integer, Competition> competitions;
	private Map<Integer, CompetitionModel> competitionModels;
	private Map<Integer, Team> teams;
	private List<Team> rankedTeams;
	private int nextId;

	protected AbstractDataController(RandomGenerator randomGenerator) {
		this.randomGenerator = randomGenerator;
		this.competitions = new HashMap<>();
		this.nextId = 1;
	}
	
	@Override
	public final Collection<Competition> getCompetitions() {
		return competitions.values();
	}
	@Override
	public final Collection<CompetitionModel> getCompetitionModels() {
		return competitionModels.values();
	}
	@Override
	public final Collection<World> getWorlds() {
		return worlds.values();
	}
	@Override
	public final Collection<Continent> getContinents() {
		return continents.values();
	}
	@Override
	public final Collection<Country> getCountries() {
		return countries.values();
	}
	@Override
	public final Collection<Team> getTeams() {
		return teams.values();
	}
	protected final void setCompetitions(Collection<Competition> competitions) {
		this.competitions = competitions.stream().collect(Collectors.toMap(c -> c.getId(), c -> c));
	}
	protected final void setTeams(Collection<Team> teams) {
		this.teams = teams.stream().collect(Collectors.toMap(t -> t.id(), t -> t));
		this.rankedTeams = teams.stream().sorted().toList();
	}
	protected final void setWorlds(Collection<World> worlds) {
		this.worlds = worlds.stream().collect(Collectors.toMap(w -> w.id(), w -> w));
	}
	protected final void setContinents(Collection<Continent> continents) {
		this.continents = continents.stream().collect(Collectors.toMap(c -> c.id(), c -> c));
	}
	protected final void setCountries(Collection<Country> countries) {
		this.countries = countries.stream().collect(Collectors.toMap(c -> c.id(), c -> c));
	}
	protected final void setCompetitionModels(Collection<CompetitionModel> competitionModels) {
		this.competitionModels = competitionModels.stream().collect(Collectors.toMap(c -> c.getId(), c -> c));
	}
	@Override
	public final Optional<World> getWorld(String name) {
		return worlds.values().stream().filter(w -> w.name().equals(name)).findAny();
	}
	@Override
	public final Optional<World> getWorld(int id) {
		return Optional.ofNullable(worlds.get(id));
	}
	@Override
	public final Optional<Continent> getContinent(String name) {
		return continents.values().stream().filter(c -> c.name().equals(name)).findAny();
	}
	@Override
	public final Optional<Continent> getContinent(int id) {
		return Optional.ofNullable(continents.get(id));
	}
	@Override
	public final Optional<Country> getCountry(String name) {
		return countries.values().stream().filter(c -> c.name().equals(name)).findAny();
	}
	@Override
	public final Optional<Country> getCountry(int id) {
		return Optional.ofNullable(countries.get(id));
	}
	@Override
	public final Optional<Team> getTeam(String name) {
		return teams.values().stream().filter(t -> t.name().equals(name)).findAny();
	}
	@Override
	public final Optional<Team> getTeam(int id) {
		return Optional.ofNullable(teams.get(id));
	}
	@Override
	public final Optional<Competition> getCompetition(String name) {
		return competitions.values().stream().filter(c -> c.getName().equals(name)).findAny();
	}
	@Override
	public final Optional<Competition> getCompetition(int id) {
		return Optional.ofNullable(competitions.get(id));
	}
	@Override
	public Optional<Pool> getPool(int id) {
		return competitions.values().stream()
				.map(c -> c.getPools())
				.flatMap(p -> p.stream())
				.filter(p -> p.getId() == id)
				.findAny();
	}
	@Override
	public final Optional<CompetitionModel> getCompetitionModel(String name) {
		return competitionModels.values().stream().filter(c -> c.getName().equals(name)).findAny();
	}
	@Override
	public final Optional<CompetitionModel> getCompetitionModel(int id) {
		return Optional.ofNullable(competitionModels.get(id));
	}
	@Override
	public Optional<Game> getGame(int id) {
		return competitions.values().stream()
				.map(c -> c.getPools())
				.flatMap(p -> p.stream())
				.map(p -> p.getGames())
				.flatMap(g -> g.stream())
				.filter(g -> g.getId() == id)
				.findAny();
		
	}
	@Override
	public final void add(Competition competition) {
		competitions.put(competition.getId(), competition);
	}
	@Override
	public final RandomGenerator getRandomGenerator() {
		return randomGenerator;
	}
	@Override
	public int getNextId() {
		return nextId++;
	}
	@Override
	public final Ranking getRanking(Team team) {
		int position = getPosition(team);
		int pct = position * 100 / teams.size();
		if(pct <= 10)
			return new Ranking(position, RankingClass.FAVORITE);
		if(pct <= 40)
			return new Ranking(position, RankingClass.OUTSIDER);
		if(pct <= 60)
			return new Ranking(position, RankingClass.AVERAGE);
		if(pct <= 90)
			return new Ranking(position, RankingClass.UNEXPECTED);
		return new Ranking(position, RankingClass.BOTTOM);
	}
	protected final int getPosition(Team team) {
		return rankedTeams.indexOf(team)+1;
	}
}
