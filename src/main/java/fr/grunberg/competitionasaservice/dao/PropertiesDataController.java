package fr.grunberg.competitionasaservice.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;
import java.util.SequencedCollection;

import org.springframework.stereotype.Service;

import fr.grunberg.competitionasaservice.competition.model.CompetitionModel;
import fr.grunberg.competitionasaservice.competition.model.CompetitionStageGroup;
import fr.grunberg.competitionasaservice.competition.model.CompetitionStageModel;
import fr.grunberg.competitionasaservice.competition.model.CompetitionStageType;
import fr.grunberg.competitionasaservice.geography.Attribute;
import fr.grunberg.competitionasaservice.geography.Continent;
import fr.grunberg.competitionasaservice.geography.Country;
import fr.grunberg.competitionasaservice.geography.Location;
import fr.grunberg.competitionasaservice.geography.World;
import fr.grunberg.competitionasaservice.team.Player;
import fr.grunberg.competitionasaservice.team.Team;

@Service
public final class PropertiesDataController extends AbstractDataController {
	private static final int PLAYER_ORDINARY_MIN_SCORE_DEFENSE = 1;
	private static final int PLAYER_ORDINARY_MAX_SCORE_DEFENSE = 5;
	private static final int PLAYER_ORDINARY_MIN_SCORE_ATTACK = 1;
	private static final int PLAYER_ORDINARY_MAX_SCORE_ATTACK = 6;
	
	private static final String defaultGeographyFile = "geography-hyboria.properties";
	
	private final Properties properties;
	
	private int luckFactor;
	
	public PropertiesDataController() {
		super(new Random(1));
		properties = new Properties();
		loadFile("sport.properties");
		loadFile("geography.properties");
		if(properties.containsKey("geography.file1")) {
			int fileNumber = 1;
			while(properties.containsKey("geography.file" + fileNumber)) {
				loadFile(properties.getProperty("geography.file" + fileNumber));
				fileNumber++;
			}
		}
		else {
			loadFile(defaultGeographyFile);
		}
		loadFile("teams.properties");
		loadFile("competitions.properties");
		System.out.println(properties.size() + " properties loaded.");
		initialize();
	}
	
	private void loadFile(String filePath) {
		System.out.println("Loading " + filePath);
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath)) {
			properties.load(is);
			System.out.println("Loaded " + filePath);
		} catch (IOException e) {
			System.err.println("Failed to read " + filePath);
			e.printStackTrace();
		}
	}
	
	private void initialize() {
		Collection<String> worldShortNames = getAllShortNames("geography.world.");
		Collection<World> worlds = worldShortNames.stream().map(sn -> decodeWorld(sn)).toList();
		setWorlds(worlds);
		System.out.println("Loaded " + getWorlds().size() + " worlds.");
		
		Collection<String> continentShortNames = getAllShortNames("geography.continent.");
		Collection<Continent> continents = continentShortNames.stream().map(sn -> decodeContinent(sn)).toList();
		setContinents(continents);
		System.out.println("Loaded " + getContinents().size() + " continents.");

		Collection<String> countryShortNames = getAllShortNames("geography.country.");
		Collection<Country> countries = countryShortNames.stream().map(sn -> decodeCountry(sn)).toList();
		setCountries(countries);
		System.out.println("Loaded " + getCountries().size() + " countries.");
		
		if(Boolean.parseBoolean((String) properties.getOrDefault("sport.team.onepercountry", "false"))) {
			setTeams(getCountries().stream()
					.map(c -> new Team(getNextId(), c.name(), c, createPlayers(c, Integer.parseInt(properties.getProperty("sport.team.players")))))
					.toList()
					);
			System.out.println("Loaded " + getTeams().size() + " teams.");
		}
		
		Collection<String> competitionShortNames = getAllShortNames("competition.");
		Collection<CompetitionModel> competitionModels = competitionShortNames.stream().map(c -> decodeCompetition(c)).toList();
		setCompetitionModels(competitionModels);
		setCompetitions(new ArrayList<>());
		System.out.println("Loaded " + getCompetitionModels().size() + " competition models.");
		
		luckFactor = Integer.parseInt(properties.getProperty("sport.luck-factor"));
	}

	private CompetitionModel decodeCompetition(String shortName) {
		String prefix = "competition.";
		String name = properties.getProperty(prefix + shortName + ".name");
		Optional<? extends Location> location = decodeLocation(
				properties.getProperty(prefix + shortName + ".world"),
				properties.getProperty(prefix + shortName + ".continent")
				);
		if(CompetitionStageType.CHAMPIONSHIP.name().equalsIgnoreCase(properties.getProperty(prefix + shortName + ".type"))) {
			return new CompetitionModel(getNextId(), name,
					List.of(new CompetitionStageModel(CompetitionStageType.CHAMPIONSHIP, null, location.orElse(null), 0))
					);
		}
		if(CompetitionStageType.ELIMINATION.name().equalsIgnoreCase(properties.getProperty(prefix + shortName + ".type"))) {
			return new CompetitionModel(getNextId(), name,
					List.of(new CompetitionStageModel(CompetitionStageType.ELIMINATION, null, location.orElse(null), 0))
					);
		}
		SequencedCollection<CompetitionStageModel> stages = new ArrayList<>();
		int stage = 1;
		Optional<CompetitionStageModel> nextStage = decodeCompetitionStage(prefix, shortName, ".stage." + stage);
		while(nextStage.isPresent()) {
			stages.add(nextStage.get());
			stage++;
			nextStage = decodeCompetitionStage(prefix, shortName, ".stage." + stage);
		}
		return new CompetitionModel(getNextId(), name, stages);
	}
	
	private Optional<CompetitionStageModel> decodeCompetitionStage(String prefix, String shortName, String suffix) {
		String typeString = properties.getProperty(prefix + shortName + suffix + ".type");
		if(typeString == null)
			return Optional.empty();
		CompetitionStageType type = CompetitionStageType.valueOf(typeString.toUpperCase());
		String groupString = properties.getProperty(prefix + shortName + suffix + ".group");
		CompetitionStageGroup group = (groupString != null ? CompetitionStageGroup.valueOf(groupString.toUpperCase()) : null);
		String selectedString = properties.getProperty(prefix + shortName + suffix + ".selected");
		int selected = 0;
		if(selectedString != null) {
			selected = Integer.parseInt(selectedString);
		}
		return Optional.of(new CompetitionStageModel(type, group, null, selected));
	}
	
	private Optional<? extends Location> decodeLocation(String worldName, String continentName) {
		if(worldName != null) {
			return getWorld(worldName);
		}
		if(continentName != null) {
			return getContinent(continentName);
		}
		return null;
	}

	private World decodeWorld(String shortName) {
		String prefix = "geography.world.";
		return new World(getNextId(), properties.getProperty(prefix + shortName + ".name"));
	}
	private Continent decodeContinent(String shortName) {
		String prefix = "geography.continent.";
		Optional<World> world = this.getWorld(properties.getProperty(prefix + shortName + ".world"));
		if(world.isEmpty())
			throw new RuntimeException("Continent " + shortName + " is missing its world " + properties.getProperty(prefix + shortName + ".world"));
		return new Continent(getNextId(), properties.getProperty(prefix + shortName + ".name"), world.get());
	}
	private Country decodeCountry(String shortName) {
		String prefix = "geography.country.";
		Optional<Continent> continent = this.getContinent(properties.getProperty(prefix + shortName + ".continent"));
		if(continent.isEmpty())
			throw new RuntimeException("Country " + shortName + " is missing its continent " + properties.getProperty(prefix + shortName + ".continent"));
		String attributesAsString = properties.getProperty(prefix + shortName + ".attributes");
		return new Country(getNextId(), properties.getProperty(prefix + shortName + ".name"), continent.get(), decodeAttributes(attributesAsString));
	}
	private Collection<Attribute> decodeAttributes(String attributesAsString) {
		return Arrays.stream(attributesAsString.split(","))
				.map(a -> a.trim())
				.filter(Objects::nonNull)
				.filter(a -> !a.isBlank())
				.map(a -> decodeAttribute(a).orElse(null))
				.filter(Objects::nonNull)
				.toList();
	}
	private Optional<Attribute> decodeAttribute(String attribute) {
		Optional<Attribute> attributeFound = Arrays.stream(Attribute.values())
				.filter(a -> a.name().equalsIgnoreCase(attribute))
				.findAny();
		if(attributeFound.isEmpty())
			System.err.println("Attribute " + attribute + " was not found");
		return attributeFound;
	}

	private Collection<String> getAllShortNames(String prefix) {
		return properties.keySet().stream()
				.map(k -> (String) k)
				.filter(k -> k.startsWith(prefix))
				.map(k -> k.replace(prefix, "").split("\\.", 0)[0])
				.distinct()
				.toList();
	}
	private Collection<Player> createPlayers(Country country, int number) {
		Collection<Player> players = new ArrayList<>(number);
		for(Attribute attribute : country.attributes()) {
			switch(attribute) {
			case ADVANCED:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE-1, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK+1));
				break;
			case AGGRESSIVE:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE-1, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK));
				break;
			case DECADENT:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK-1));
				break;
			case DEFENSIVE:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE+1, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK-1));
				break;
			case ISLAND:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK));
				break;
			case LARGE:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK-1));
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE-1, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK));
				break;
			case LOYAL:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK+1));
				break;
			case MARITIME:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK));
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE-1, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK+1));
				break;
			case MAGIC:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE+1, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK+1));
				break;
			case MERCHANT:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK));
				break;
			case ORGANIZED:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK+1));
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE+1, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK));
				break;
			case SMALL:
				break;
			case STEPPES:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK-1));
				break;
			case TRIBE:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MIN_SCORE_ATTACK));
				break;
			case WEALTHY:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK-1));
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK-1));
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MAX_SCORE_DEFENSE-1, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK));
				break;
			case WILD:
				players.add(new Player(getNextId(), 
						PLAYER_ORDINARY_MIN_SCORE_DEFENSE, 
						PLAYER_ORDINARY_MAX_SCORE_ATTACK-1));
				break;
			default:
				break;
			}
		}
		while(players.size() < number) {
			players.add(new Player(getNextId(),
					getRandomGenerator().nextInt(PLAYER_ORDINARY_MIN_SCORE_DEFENSE, PLAYER_ORDINARY_MAX_SCORE_DEFENSE),
					getRandomGenerator().nextInt(PLAYER_ORDINARY_MIN_SCORE_ATTACK, PLAYER_ORDINARY_MAX_SCORE_ATTACK)
					));
		}
		return Collections.unmodifiableCollection(players);
	}

	@Override
	public int getLuckFactor() {
		return luckFactor;
	}
}
