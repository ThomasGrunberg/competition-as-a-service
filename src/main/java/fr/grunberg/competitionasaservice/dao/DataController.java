package fr.grunberg.competitionasaservice.dao;

import java.util.Collection;
import java.util.Optional;
import java.util.random.RandomGenerator;

import fr.grunberg.competitionasaservice.competition.Competition;
import fr.grunberg.competitionasaservice.competition.Game;
import fr.grunberg.competitionasaservice.competition.Pool;
import fr.grunberg.competitionasaservice.competition.model.CompetitionModel;
import fr.grunberg.competitionasaservice.geography.Continent;
import fr.grunberg.competitionasaservice.geography.Country;
import fr.grunberg.competitionasaservice.geography.World;
import fr.grunberg.competitionasaservice.team.Ranking;
import fr.grunberg.competitionasaservice.team.Team;

public interface DataController {
	public Collection<Competition> getCompetitions();
	public Collection<CompetitionModel> getCompetitionModels();
	public Collection<World> getWorlds();
	public Collection<Continent> getContinents();
	public Collection<Country> getCountries();
	public Collection<Team> getTeams();
	public Optional<World> getWorld(String name);
	public Optional<World> getWorld(int id);
	public Optional<Continent> getContinent(String name);
	public Optional<Continent> getContinent(int id);
	public Optional<Country> getCountry(String name);
	public Optional<Country> getCountry(int id);
	public Optional<Team> getTeam(String name);
	public Optional<Team> getTeam(int id);
	public Optional<Competition> getCompetition(String name);
	public Optional<Competition> getCompetition(int id);
	public Optional<Pool> getPool(int id);
	public Optional<CompetitionModel> getCompetitionModel(String name);
	public Optional<CompetitionModel> getCompetitionModel(int competitionModelId);
	public Optional<Game> getGame(int id);
	public Ranking getRanking(Team team);
	public RandomGenerator getRandomGenerator();
	public int getLuckFactor();
	public int getNextId();
	public void add(Competition competition);
}
