package fr.grunberg.competitionasaservice.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.TeamDto;

@RestController
public final class TeamController {
	@Autowired
	private DataController dataController;

    @GetMapping("/api/teams")
    public Collection<TeamDto> getTeams() {
    	return dataController.getTeams().stream()
    			.sorted()
    			.map(c -> c.toDto(dataController))
    			.toList();
    }
}
