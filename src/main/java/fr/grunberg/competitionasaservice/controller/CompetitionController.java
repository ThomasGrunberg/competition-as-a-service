package fr.grunberg.competitionasaservice.controller;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.grunberg.competitionasaservice.competition.Competition;
import fr.grunberg.competitionasaservice.competition.Game;
import fr.grunberg.competitionasaservice.competition.Pool;
import fr.grunberg.competitionasaservice.competition.model.CompetitionModel;
import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.dto.CompetitionDto;
import fr.grunberg.competitionasaservice.dto.CompetitionModelDto;
import fr.grunberg.competitionasaservice.dto.CreateCompetitionDto;
import fr.grunberg.competitionasaservice.dto.PoolDto;

@RestController
public final class CompetitionController {
	@Autowired
	private DataController dataController;

    @GetMapping("/api/competitions")
    public Collection<CompetitionDto> getCompetitions() {
    	return dataController.getCompetitions().stream().map(c -> c.toDto(dataController)).toList();
    }
    @GetMapping("/api/competitions/{id}")
    public CompetitionDto getCompetition(@PathVariable int id) {
    	return dataController.getCompetition(id)
    			.map(c -> c.toDto(dataController))
    			.orElse(null);
    }
    @GetMapping("/api/competitionmodels")
    public Collection<CompetitionModelDto> getCompetitionModels() {
    	return dataController.getCompetitionModels().stream().map(c -> c.toDto(dataController)).toList();
    }
    @PostMapping("/api/competitions")
    public CompetitionDto createCompetitionFromModel(@RequestBody CreateCompetitionDto createCompetitionDto) {
    	Optional<CompetitionModel> model = dataController.getCompetitionModel(createCompetitionDto.competitionModelId());
    	if(model.isEmpty())
    		return null;
		Optional<ChronoLocalDate> lastDate = dataController.getCompetitions().stream()
				.filter(c -> c.getModel().equals(model.get()))
				.map(c -> c.getStartDate())
				.max(Comparator.naturalOrder());
		ChronoLocalDate startDate;
		if(lastDate.isEmpty())
			startDate = LocalDate.now();
		else
			startDate = lastDate.get().plus(1, ChronoUnit.YEARS);
    	Competition competition = model.get().instantiate(dataController, startDate);
    	return competition.toDto(dataController);
    }
    @PostMapping("/api/games/{id}")
    public PoolDto playGame(@PathVariable int id) {
    	Optional<Game> oGame = dataController.getGame(id);
    	if(oGame.isEmpty())
    		return null;
    	Game game = oGame.get();
    	game.play(dataController);
    	game.getPool().recalculateStatistics();
    	return game.getPool().toDto(dataController);
    }
    @PostMapping("/api/pools/{id}")
    public PoolDto playAllGames(@PathVariable int id) {
    	Optional<Pool> oPool = dataController.getPool(id);
    	if(oPool.isEmpty())
    		return null;
    	Pool pool = oPool.get();
    	pool.getGames()
    			.stream()
    			.filter(g -> !g.isFinished())
    			.forEach(g -> g.play(dataController));
    	pool.recalculateStatistics();
    	return pool.toDto(dataController);
    }
}
