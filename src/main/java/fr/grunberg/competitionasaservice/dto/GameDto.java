package fr.grunberg.competitionasaservice.dto;

import java.time.chrono.ChronoLocalDate;

public record GameDto(
		int id,
		TeamSpotDto homeTeam, 
		TeamSpotDto awayTeam,
		boolean finished,
		String result,
		int homeGoals,
		int awayGoals,
		ChronoLocalDate date) 
implements BeanDto {
}
