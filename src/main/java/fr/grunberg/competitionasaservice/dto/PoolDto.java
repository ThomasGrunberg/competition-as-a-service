package fr.grunberg.competitionasaservice.dto;

import java.util.Collection;
import java.util.SequencedCollection;

public record PoolDto(
		int id,
		String name, 
		Collection<GameDto> games,
		Collection<TeamSpotDto> teams,
		SequencedCollection<PoolStatisticsDto> statistics,
		boolean finished
		) implements BeanDto {
}
