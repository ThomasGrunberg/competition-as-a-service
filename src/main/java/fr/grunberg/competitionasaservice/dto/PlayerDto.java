package fr.grunberg.competitionasaservice.dto;

public record PlayerDto (int id, int defense, int attack)
	implements BeanDto  {
}