package fr.grunberg.competitionasaservice.dto;

import java.time.chrono.ChronoLocalDate;

public record TeamSpotDto(
		int id,
		Integer originPoolId,
		Integer originPoolPosition,
		String originPoolName,
		TeamDto team,
		ChronoLocalDate firstDate
		) 
implements BeanDto {
}
