package fr.grunberg.competitionasaservice.dto;

public record PoolStatisticsDto (
		TeamSpotDto team,
		int points,
		int goalsScored,
		int goalsReceived,
		int victories,
		int draws,
		int losses,
		int unplayed,
		boolean selected
		) implements BeanDto {
}
