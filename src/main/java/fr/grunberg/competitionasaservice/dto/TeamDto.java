package fr.grunberg.competitionasaservice.dto;

import java.util.Collection;

public record TeamDto(int id, 
		String name,
		int defense,
		int attack,
		int ranking,
		String rankingClass,
		String continentName,
		String worldName,
		Collection<PlayerDto> players
		) 
implements BeanDto {
}
