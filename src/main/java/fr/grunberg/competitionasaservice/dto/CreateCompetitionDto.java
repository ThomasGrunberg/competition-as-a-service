package fr.grunberg.competitionasaservice.dto;

public record CreateCompetitionDto(int competitionModelId) {
}
