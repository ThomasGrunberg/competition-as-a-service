package fr.grunberg.competitionasaservice.dto;

import fr.grunberg.competitionasaservice.dao.DataController;

public interface Transferable<T extends BeanDto> {
	public T toDto(DataController dataController);
}