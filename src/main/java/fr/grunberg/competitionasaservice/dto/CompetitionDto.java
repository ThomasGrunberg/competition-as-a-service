package fr.grunberg.competitionasaservice.dto;

import java.util.SequencedCollection;

public record CompetitionDto(int id, String name, String restriction, SequencedCollection<PoolDto> pools) implements BeanDto {
}
