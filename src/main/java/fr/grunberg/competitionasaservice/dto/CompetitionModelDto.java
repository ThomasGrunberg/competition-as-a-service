package fr.grunberg.competitionasaservice.dto;

public record CompetitionModelDto(int id, String name) implements BeanDto {
}
