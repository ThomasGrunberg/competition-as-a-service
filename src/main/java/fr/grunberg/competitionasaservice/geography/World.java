package fr.grunberg.competitionasaservice.geography;

public record World(int id, String name) implements Location {}