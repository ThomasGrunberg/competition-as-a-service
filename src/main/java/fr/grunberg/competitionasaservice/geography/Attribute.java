package fr.grunberg.competitionasaservice.geography;

public enum Attribute {
	ADVANCED,
	AGGRESSIVE,
	DECADENT,
	DEFENSIVE,
	ISLAND,
	LARGE,
	LOYAL,
	MAGIC,
	MARITIME,
	MERCHANT,
	ORGANIZED,
	SMALL,
	STEPPES,
	TRIBE,
	WEALTHY,
	WILD,
}
