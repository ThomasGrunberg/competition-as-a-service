package fr.grunberg.competitionasaservice.geography;

import java.util.Collection;

public record Country(int id, String name, Continent continent, Collection<Attribute> attributes) implements Location {}