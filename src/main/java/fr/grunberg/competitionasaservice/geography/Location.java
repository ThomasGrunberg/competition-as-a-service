package fr.grunberg.competitionasaservice.geography;

public interface Location {
	public String name();
	public int id();
}
