package fr.grunberg.competitionasaservice.geography;

public record Continent(int id, String name, World world) implements Location {}