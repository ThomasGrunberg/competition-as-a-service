import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./pages/RootLayout";
import CompetitionModelsPage from "./pages/CompetitionModelsPage";
import CompetitionsPage from "./pages/CompetitionsPage";
import CompetitionPage from "./pages/CompetitionPage";
import TeamsPage from "./pages/TeamsPage";

const routes = [
  { path: "competitionmodels", element: <CompetitionModelsPage /> },
  { path: "competitions", element: <CompetitionsPage /> },
  { path: "competitions/:id", element: <CompetitionPage /> },
  { path: "teams", element: <TeamsPage /> },
];

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: routes,
  },
  {
    path: "/competitionasaservice",
    element: <RootLayout />,
    children: routes,
  },
  {
    path: "/competitionmodels",
    element: <RootLayout />,
    children: routes,
  },
  {
    path: "/competitions",
    element: <RootLayout />,
    children: routes,
  },
  {
    path: "/competitions/:id",
    element: <RootLayout />,
    children: routes,
  },
  {
    path: "/teams",
    element: <RootLayout />,
    children: routes,
  },
]);

function App() {
  return (
    <>
      <h1>Competition as a Service</h1>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
