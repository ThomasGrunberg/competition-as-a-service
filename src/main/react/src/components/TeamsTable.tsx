import { table, thead, tbody, th, tr, td } from "../styles/TableStyles";
import TeamDto from "../dto/TeamDto";
import { useState } from "react";
import { getTeams } from "../utils/RestUtils";

function RegionsTable() {
  const [teams, setTeams] = useState(new Array<TeamDto>());
  if (teams.length == 0) {
    getTeams(setTeams);
  }

  let visibleStyle = "fadeIn";
  if (teams.length == 0) {
    visibleStyle = "fadeOut";
  }

  return (
    <div className={visibleStyle}>
      <table className={table()}>
        <thead className={thead()}>
          <tr className={tr()}>
            <th className={th()}>Name</th>
            <th className={th()}>Defense</th>
            <th className={th()}>Attack</th>
            <th className={th()}>Ranking class</th>
          </tr>
        </thead>
        <tbody className={tbody()}>
          {teams.map((team) => {
            return (
              <tr className={tr()} key={team.id}>
                <td className={td()}>{team.name}</td>
                <td className={td()}>{team.defense}</td>
                <td className={td()}>{team.attack}</td>
                <td
                  className={
                    td() + " ranking-" + team.rankingClass.toLowerCase()
                  }
                >
                  {team.rankingClass[0].toUpperCase() +
                    team.rankingClass.toLowerCase().slice(1)}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default RegionsTable;
