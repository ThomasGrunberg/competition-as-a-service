import { table, thead, tbody, th, tr, td } from "../styles/TableStyles";
import PoolDto from "../dto/PoolDto";
import GameDto from "../dto/GameDto";
import TeamSpotDto, { displayTeamSpotDto } from "../dto/TeamSpotDto";
import { playAllGames, playGame } from "../utils/RestUtils";
import { useState } from "react";
import PoolStatisticsDto from "../dto/PoolStatisticsDto";
import { getBackgroundColor } from "../dto/TeamDto";

interface Props {
  poolAsProp: PoolDto;
  refreshCompetition: () => void;
}

function PoolDetail({ poolAsProp, refreshCompetition }: Props) {
  const [poolAsState, setPool] = useState(poolAsProp);
  const poolToDisplay =
    poolAsState.freshness > poolAsProp.freshness ? poolAsState : poolAsProp;
  let teams: TeamSpotDto[] = poolToDisplay.statistics.map((s) => s.team);
  let poolPlayable =
    poolToDisplay.statistics.length == poolToDisplay.teams.length;
  if (
    teams == null ||
    teams == undefined ||
    teams.length == 0 ||
    !poolPlayable
  ) {
    teams = poolToDisplay.teams;
  }

  return (
    <>
      <h3>{poolToDisplay.name}</h3>
      {!poolToDisplay.finished && (
        <button
          onClick={() => {
            playAllGames(poolToDisplay.id, setPool);
            refreshCompetition();
          }}
        >
          Play all
        </button>
      )}
      <table className={table()}>
        <thead className={thead()}>
          <tr className={tr()}>
            <th className={th()} />
            {teams.map((team) => {
              return <th className={th()}>{displayTeamSpotDto(team)}</th>;
            })}
            <th className={th()}>Played</th>
            <th className={th()}>Victories</th>
            <th className={th()}>Draws</th>
            <th className={th()}>Losses</th>
            <th className={th()}>Points</th>
            <th className={th()}>Goals for</th>
            <th className={th()}>Goals against</th>
          </tr>
        </thead>
        <tbody className={tbody()}>
          {teams.map((team) => {
            const statistics: PoolStatisticsDto =
              poolToDisplay.statistics.filter((s) => s.team == team)[0];
            return (
              <tr className={tr()}>
                <td
                  style={{ backgroundColor: getBackgroundColor(team.team) }}
                  className={
                    td() + " " + (statistics?.selected ? "selected-team" : "")
                  }
                >
                  {displayTeamSpotDto(team)}
                </td>
                {teams.map((otherteam) => {
                  if (team.id != otherteam.id) {
                    const game = findGame(poolToDisplay, team, otherteam, true);
                    if (
                      !poolPlayable ||
                      game.homeTeam.team == null ||
                      game.awayTeam.team == null
                    ) {
                      return <td />;
                    } else if (game.finished) {
                      return (
                        <td className={td()}>
                          {game.homeTeam.id == team.id
                            ? game.homeGoals + " - " + game.awayGoals
                            : game.awayGoals + " - " + game.homeGoals}
                        </td>
                      );
                    } else {
                      return (
                        <td className={td()}>
                          <button
                            onClick={() => {
                              playGame(game.id, (poolParam: PoolDto) => {
                                setPool(poolParam);
                                if (poolParam.finished) {
                                  refreshCompetition();
                                }
                              });
                            }}
                          >
                            Play
                          </button>
                        </td>
                      );
                    }
                  } else {
                    return <td />;
                  }
                })}
                <td>
                  {statistics == null
                    ? ""
                    : statistics.victories +
                      statistics.draws +
                      statistics.losses}
                </td>
                <td>{statistics == null ? "" : statistics.victories}</td>
                <td>{statistics == null ? "" : statistics.draws}</td>
                <td>{statistics == null ? "" : statistics.losses}</td>
                <td>{statistics == null ? "" : statistics.points}</td>
                <td>{statistics == null ? "" : statistics.goalsScored}</td>
                <td>{statistics == null ? "" : statistics.goalsReceived}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

function findGame(
  pool: PoolDto,
  homeTeam: TeamSpotDto,
  awayTeam: TeamSpotDto,
  ignoreHomeAway: boolean
): GameDto {
  return pool.games.filter((g) => {
    return (
      (g.homeTeam.id == homeTeam.id && g.awayTeam.id == awayTeam.id) ||
      (g.homeTeam.id == awayTeam.id &&
        g.awayTeam.id == homeTeam.id &&
        ignoreHomeAway)
    );
  })[0];
}

export default PoolDetail;
