import { NavLink } from "react-router-dom";

function MainNavigation() {
  return (
    <header>
      <nav>
        <ul>
          <li>
            <NavLink to="competitionmodels">Competition models</NavLink>
          </li>
          <li>
            <NavLink to="competitions">Competitions</NavLink>
          </li>
          <li>
            <NavLink to="teams">Teams</NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default MainNavigation;
