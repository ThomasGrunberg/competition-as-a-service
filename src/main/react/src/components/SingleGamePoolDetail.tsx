import { table, thead, tbody, th, tr, td } from "../styles/TableStyles";
import PoolDto from "../dto/PoolDto";
import TeamSpotDto, { displayTeamSpotDto } from "../dto/TeamSpotDto";
import { playGame } from "../utils/RestUtils";
import { useState } from "react";
import { getBackgroundColor } from "../dto/TeamDto";

interface Props {
  poolAsProp: PoolDto;
  refreshCompetition: () => void;
}

function SingleGamePoolDetail({ poolAsProp, refreshCompetition }: Props) {
  const [poolAsState, setPool] = useState(poolAsProp);
  const poolToDisplay =
    poolAsState.freshness > poolAsProp.freshness ? poolAsState : poolAsProp;
  let teams: TeamSpotDto[] = poolToDisplay.statistics.map((s) => s.team);
  let poolPlayable =
    poolToDisplay.statistics.length == poolToDisplay.teams.length;
  const game = poolToDisplay.games[0];
  if (
    teams == null ||
    teams == undefined ||
    teams.length == 0 ||
    !poolPlayable
  ) {
    teams = poolToDisplay.teams;
  }

  return (
    <>
      <table className={table()}>
        <thead className={thead()}>
          <tr className={tr()}>
            <th className={th()}>{poolToDisplay.name}</th>
          </tr>
        </thead>
        <tbody className={tbody()}>
          <tr className={tr()}>
            <td
              style={{
                backgroundColor: getBackgroundColor(
                  poolToDisplay.games[0].homeTeam.team
                ),
              }}
              className={
                td() +
                (poolToDisplay.games[0].homeTeam.team != null
                  ? " ranking-" +
                    poolToDisplay.games[0].homeTeam.team.rankingClass.toLowerCase()
                  : "")
              }
            >
              {displayTeamSpotDto(poolToDisplay.games[0].homeTeam)}
            </td>
            {!poolPlayable ||
            game.homeTeam.team == null ||
            game.awayTeam.team == null ? (
              <td />
            ) : game.finished ? (
              <td className={td()}>
                {game.homeGoals + " - " + game.awayGoals}
              </td>
            ) : (
              <td className={td()}>
                <button
                  onClick={() => {
                    playGame(game.id, (pool: PoolDto) => {
                      setPool(pool);
                      if (pool.finished) {
                        refreshCompetition();
                      }
                    });
                  }}
                >
                  Play
                </button>
              </td>
            )}
            <td
              style={{
                backgroundColor: getBackgroundColor(
                  poolToDisplay.games[0].awayTeam.team
                ),
              }}
              className={td()}
            >
              {displayTeamSpotDto(poolToDisplay.games[0].awayTeam)}
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}

export default SingleGamePoolDetail;
