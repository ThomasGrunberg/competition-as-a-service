import { table, thead, tbody, th, tr, td } from "../styles/TableStyles";
import { useState } from "react";
import { getCompetitions } from "../utils/RestUtils";
import CompetitionDto from "../dto/CompetitionDto";
import { Link } from "react-router-dom";

function CompetitionsTable() {
  const [competitions, setCompetitions] = useState(new Array<CompetitionDto>());
  if (competitions.length == 0) {
    getCompetitions(setCompetitions);
  }

  let visibleStyle = "fadeIn";
  if (competitions.length == 0) {
    visibleStyle = "fadeOut";
  }

  return (
    <>
      <table className={table() + " " + visibleStyle}>
        <thead className={thead()}>
          <tr className={tr()}>
            <th className={th()}>Name</th>
            <th className={th()}>Winner</th>
            <th className={th()}></th>
          </tr>
        </thead>
        <tbody className={tbody()}>
          {competitions.map((competition) => {
            return (
              <tr className={tr()} key={competition.id}>
                <td className={td()}>{competition.name}</td>
                <td className={td()}></td>
                <Link to={`/competitions/${competition.id}`}>Show</Link>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default CompetitionsTable;
