import { useState } from "react";
import CompetitionDto from "../dto/CompetitionDto";
import { getCompetition } from "../utils/RestUtils";
import PoolDetail from "./PoolDetail";
import SingleGamePoolDetail from "./SingleGamePoolDetail";

interface Props {
  id: number;
}

function CompetitionDetails({ id }: Props) {
  const [competition, setCompetition] = useState<CompetitionDto | null>(null);
  if (competition == null) {
    getCompetition(id, setCompetition);
    return "";
  }

  let visibleStyle = "fadeIn";
  const refreshCompetition = () => {
    getCompetition(id, setCompetition);
  };

  return (
    <div className={visibleStyle}>
      <h3>{competition.name}</h3>
      {competition.pools.map((pool) => {
        if (pool.games.length == 1) {
          return (
            <SingleGamePoolDetail
              poolAsProp={pool}
              refreshCompetition={refreshCompetition}
            />
          );
        } else {
          return (
            <PoolDetail
              poolAsProp={pool}
              refreshCompetition={refreshCompetition}
            />
          );
        }
      })}
    </div>
  );
}

export default CompetitionDetails;
