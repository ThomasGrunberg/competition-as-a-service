import { table, thead, tbody, th, tr, td } from "../styles/TableStyles";
import CompetitionModelDto from "../dto/CompetitionModelDto";
import { useState } from "react";
import {
  getCompetitionModels,
  createCompetitionFromModel,
} from "../utils/RestUtils";

function CompetitionModelsTable() {
  const [competitionModels, setCompetitionModels] = useState(
    new Array<CompetitionModelDto>()
  );
  if (competitionModels.length == 0) {
    getCompetitionModels(setCompetitionModels);
  }

  let visibleStyle = "fadeIn";
  if (competitionModels.length == 0) {
    visibleStyle = "fadeOut";
  }

  return (
    <div className={visibleStyle}>
      <table className={table()}>
        <thead className={thead()}>
          <tr className={tr()}>
            <th className={th()}>Name</th>
            <th className={th()}></th>
          </tr>
        </thead>
        <tbody className={tbody()}>
          {competitionModels.map((competitionModel) => {
            return (
              <tr className={tr()} key={competitionModel.id}>
                <td className={td()}>{competitionModel.name}</td>
                <td className={td()}>
                  <button
                    type="button"
                    className="btn btn-info"
                    onClick={() =>
                      createCompetitionFromModel(competitionModel.id)
                    }
                  >
                    Create
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default CompetitionModelsTable;
