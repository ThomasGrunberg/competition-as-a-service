export const RESULT_HOME_VICTORY = "HOME_VICTORY";
export const RESULT_AWAY_VICTORY = "AWAY_VICTORY";
export const RESULT_DRAW = "DRAW";
export const RESULT_UNPLAYED = "UNPLAYED";
export const RESULT_VICTORY = "VICTORY";
export const RESULT_LOSS = "LOSS";
