import CompetitionModelDto from "../dto/CompetitionModelDto";
import CompetitionDto from "../dto/CompetitionDto";
import PoolDto from "../dto/PoolDto";
import TeamDto from "../dto/TeamDto";

function getRESTArray<T>(url:string, onReceive: (arg:T[]) => void, onCall?: () => void, onError?: () => void) {
    if(typeof(onCall) !== 'undefined') {
      onCall();
    }
    fetch(url)
    .then((response) => response.json() as Promise<Array<T>>)
    .then((data) => onReceive(data))
    .catch((err) => {
      console.log(err.message);
      if(typeof(onError) !== 'undefined') {
        onError();
      }
    });
  }
  
function getRESTObject<T>(url:string, onReceive: (arg:T) => void, onCall?: () => void, onError?: () => void) {
  if(typeof(onCall) !== 'undefined') {
    onCall();
  }
  fetch(url)
    .then((response) => response.json() as Promise<T>)
    .then((data) => onReceive(data))
    .catch((err) => {
    console.log(err.message);
    if(typeof(onError) !== 'undefined') {
      onError();
    }
    });
}

function post<T>(
  url:string, 
  parameters:any, 
  onReceive?: (arg:T) => void, 
  onCall?: () => void, 
  onError?: () => void
  ) :void {
  if(typeof(onCall) !== 'undefined') {
    onCall();
  }
  fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(parameters)
  })
    .then((response) => response.json() as Promise<T>)
    .then((data) => {
      if(typeof(onReceive) !== 'undefined') {
        onReceive(data);
      }})
    .catch((err) => {
    console.log(err.message);
    if(typeof(onError) !== 'undefined') {
        onError();
    }
  });
}

export function getCompetitionModels(setCompetitionModels: (competitionmodels: CompetitionModelDto[]) => void) {
  getRESTArray("/api/competitionmodels", setCompetitionModels);
}
export function getCompetitions(setCompetitions: (competitions: CompetitionDto[]) => void) {
  getRESTArray("/api/competitions", setCompetitions);
}
export function getCompetition(id: number, setCompetition: (competition: CompetitionDto) => void) {
  getRESTObject("/api/competitions/" + id, (competition: CompetitionDto) => {
    setCompetitionFreshness(competition);
    setCompetition(competition);
  }
  );
}
export function getTeams(setTeams: (teams: TeamDto[]) => void) {
  getRESTArray("/api/teams", setTeams);
}
export function getTeam(id: number, setTeam: (teams: TeamDto) => void) {
  getRESTObject("/api/teams/" + id, setTeam);
}
export function createCompetitionFromModel(idModel: number) {
  post("/api/competitions", {competitionModelId: idModel});
}
export function playGame(id: number, setPool: (pool: PoolDto) => void) {
  post("/api/games/" + id, null, (pool: PoolDto) => {
    setPoolFreshness(pool);
    setPool(pool);
  });
}
export function playAllGames(id: number, setPool: (pool: PoolDto) => void) {
  post("/api/pools/" + id, null, (pool: PoolDto) => {
    setPoolFreshness(pool);
    setPool(pool);
  });
}

function setCompetitionFreshness(competition: CompetitionDto) {
  competition.pools.map((pool) => {
    pool.freshness = new Date();
  });
}
function setPoolFreshness(pool: PoolDto) {
    pool.freshness = new Date();
}
