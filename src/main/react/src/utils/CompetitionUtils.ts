import PoolDto from "../dto/PoolDto";
import TeamSpotDto from "../dto/TeamSpotDto";
import GameDto from "../dto/GameDto";
import {
    RESULT_HOME_VICTORY,
    RESULT_AWAY_VICTORY,
    RESULT_DRAW,
    RESULT_UNPLAYED,
    RESULT_VICTORY,
    RESULT_LOSS,
  } from "../utils/GameResult";
  
export function findGames(pool: PoolDto, team: TeamSpotDto): Array<GameDto> {
    return pool.games.filter((g) => {
      return g.homeTeam.id == team.id || g.awayTeam.id == team.id;
    });
  }
  
export function isResult(game: GameDto, team: TeamSpotDto, result: string): boolean {
    if (result == RESULT_UNPLAYED && game.result == RESULT_UNPLAYED) return true;
    if (result == RESULT_DRAW && game.result == RESULT_DRAW) return true;
    if (
        result == RESULT_VICTORY &&
        game.result == RESULT_HOME_VICTORY &&
        game.homeTeam.id == team.id
    )
        return true;
    if (
        result == RESULT_VICTORY &&
        game.result == RESULT_AWAY_VICTORY &&
        game.awayTeam.id == team.id
    )
        return true;
    if (
        result == RESULT_LOSS &&
        game.result == RESULT_HOME_VICTORY &&
        game.awayTeam.id == team.id
    )
        return true;
    if (
        result == RESULT_LOSS &&
        game.result == RESULT_AWAY_VICTORY &&
        game.homeTeam.id == team.id
    )
        return true;
    return false;
}

  