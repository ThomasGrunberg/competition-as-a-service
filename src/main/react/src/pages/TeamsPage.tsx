import TeamsTable from "../components/TeamsTable";

function TeamsPage() {
  return (
    <>
      <h2>Teams</h2>
      <TeamsTable />
    </>
  );
}

export default TeamsPage;
