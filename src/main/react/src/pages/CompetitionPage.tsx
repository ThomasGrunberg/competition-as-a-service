import { useParams } from "react-router-dom";
import CompetitionDetails from "../components/CompetitionDetails";

function CompetitionPage() {
  const { id } = useParams();
  if (id === undefined) return "";
  let idAsNumber = parseInt(id);
  return (
    <>
      <CompetitionDetails id={idAsNumber} />
    </>
  );
}

export default CompetitionPage;
