import CompetitionsTable from "../components/CompetitionsTable";

function CompetitionsPage() {
  return (
    <>
      <h2>Competitions</h2>
      <CompetitionsTable />
    </>
  );
}

export default CompetitionsPage;
