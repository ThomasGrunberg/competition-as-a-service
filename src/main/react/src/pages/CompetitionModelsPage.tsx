import CompetitionModelsTable from "../components/CompetitionModelsTable";

function CompetitionModelsPage() {
  return (
    <>
      <h2>Competition Models</h2>
      <CompetitionModelsTable />
    </>
  );
}

export default CompetitionModelsPage;
