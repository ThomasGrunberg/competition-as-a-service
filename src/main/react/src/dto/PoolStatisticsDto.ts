import TeamSpotDto from './TeamSpotDto';

type PoolStatisticsDto = {
  team: TeamSpotDto;
  points: number;
  goalsScored: number;
  goalsReceived: number;
  victories: number;
  draws: number;
  losses: number;
  unplayed: number;
  selected: boolean;
  };

export default PoolStatisticsDto;
