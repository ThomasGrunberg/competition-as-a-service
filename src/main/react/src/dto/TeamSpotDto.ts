import PoolDto from './PoolDto';
import TeamDto from './TeamDto';

type TeamSpotDto = {
    id: number;
    originPoolId: number|null;
    originPoolPosition: number|null;
    originPoolName: string|null;
    team: TeamDto|null;
    originPool: PoolDto|null;
  };

export function displayTeamSpotDto(teamSpotDto: TeamSpotDto): string {
  if(teamSpotDto.team != null)
    return teamSpotDto.team.name;
  else
    return teamSpotDto.originPoolName + " (" + teamSpotDto.originPoolPosition + ")";
}
  
export default TeamSpotDto;
