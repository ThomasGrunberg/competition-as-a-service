import PoolDto from "./PoolDto";

type CompetitionDto = {
    id: number;
    name: string;
    restriction: string|null;
    pools: PoolDto[];
  };
  
export default CompetitionDto;
