type CompetitionModelDto = {
    id: number;
    name: string;
  };
  
export default CompetitionModelDto;
