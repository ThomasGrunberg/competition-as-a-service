import TeamSpotDto from "./TeamSpotDto";

type GameDto = {
    id: number;
		homeTeam: TeamSpotDto; 
		awayTeam: TeamSpotDto;
		finished: boolean;
		result: string;
		homeGoals: number;
		awayGoals: number;
		date: Date;
  };
  
export default GameDto;
