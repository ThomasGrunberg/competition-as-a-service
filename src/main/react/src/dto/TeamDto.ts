import PlayerDto from './PlayerDto';

type TeamDto = {
    id: number;
    name: string;
    defense: number;
    attack: number;
    ranking: number;
    rankingClass: string;
    continentName: string;
    worldName: string;
    players: Array<PlayerDto>;
  };

export function getBackgroundColor(team: TeamDto|null): string {
  if(team == null)
    return "0xffffff";
  let hash = 0;
  team.continentName.split('').forEach(char => {
    hash = char.charCodeAt(0) + ((hash << 5) - hash);
  })
  let colour = '#';
  for (let i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 0xff | 0x80;
    colour += value.toString(16).padStart(2, '0');
  }
  return colour
}

export default TeamDto;
