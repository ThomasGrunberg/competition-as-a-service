type PlayerDto = {
    id: number;
    defense: number;
    attack: number;
  };
  
export default PlayerDto;
