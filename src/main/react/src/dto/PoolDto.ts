import GameDto from './GameDto';
import PoolStatisticsDto from './PoolStatisticsDto';
import TeamSpotDto from './TeamSpotDto';

type PoolDto = {
    id: number;
    name: string;
    games: GameDto[];
    teams: TeamSpotDto[];
    statistics: PoolStatisticsDto[];
    finished: boolean;
    freshness: Date;
  };
  
export default PoolDto;
