package fr.grunberg.competitionasaservice.competition;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.time.LocalDate;
import java.util.Collections;
import java.util.random.RandomGenerator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.grunberg.competitionasaservice.team.Team;

@ExtendWith(MockitoExtension.class)
public class GameTest {
	Team teamA = new Team(1, "A", null, Collections.emptyList());
	Team teamB = new Team(2, "B", null, Collections.emptyList());

	@Mock
	private RandomGenerator randomGenerator;
	
	@InjectMocks
	private Game game = new Game(1, new TeamSpot(2, teamA, null), new TeamSpot(3, teamB, null), LocalDate.now());
	
	@Test
	public void isNotFinished() {
		assertFalse(game.isFinished());
	}
}
