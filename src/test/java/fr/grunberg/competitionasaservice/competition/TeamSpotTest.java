package fr.grunberg.competitionasaservice.competition;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.random.RandomGenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.team.Team;

@ExtendWith(MockitoExtension.class)
public class TeamSpotTest {
	@Mock
	private RandomGenerator randomGenerator;
	private DataController dataController;

	@InjectMocks
	Game game = new Game(7, 
			new TeamSpot(1, 
					new Team(1, "A", null, Collections.emptyList()), LocalDate.now()), 
			new TeamSpot(2, 
					new Team(2, "B", null, Collections.emptyList()), LocalDate.now()), 
			LocalDate.now());
	
	private TeamSpot teamSpot;
	private Pool originPool;

	@BeforeEach
	public void setUp() {
		dataController = new InMemoryDataController(randomGenerator, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
		originPool = new Pool(1, "Test pool", List.of(game));
	}
	
	@Test
	public void isNotDecided() {
		teamSpot = new TeamSpot(3, originPool, 1, LocalDate.now());
		assertFalse(teamSpot.isDecided());
	}
	@Test
	public void isDecided() {
		originPool.getGames().iterator().next().play(dataController);
		teamSpot = new TeamSpot(8, originPool, 1, LocalDate.now());
		assertTrue(teamSpot.isDecided());
		assertNotNull(teamSpot.getTeam().get());
		assertNotNull(teamSpot.getTeam().get()); // Twice, because the second path is different
	}
}
