package fr.grunberg.competitionasaservice.competition;

import java.util.Collection;
import java.util.List;
import java.util.random.RandomGenerator;

import fr.grunberg.competitionasaservice.dao.AbstractDataController;
import fr.grunberg.competitionasaservice.geography.Continent;
import fr.grunberg.competitionasaservice.geography.Country;
import fr.grunberg.competitionasaservice.geography.World;
import fr.grunberg.competitionasaservice.team.Team;

public class InMemoryDataController extends AbstractDataController {

	public InMemoryDataController(RandomGenerator randomGenerator, List<World> worlds, List<Continent> continents, Collection<Country> countries, Collection<Team> teams) {
		super(randomGenerator);
		super.setWorlds(worlds);
		super.setContinents(continents);
		super.setCountries(countries);
		super.setTeams(teams);
	}

	@Override
	public int getLuckFactor() {
		return 10;
	}
}
