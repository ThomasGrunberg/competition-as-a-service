package fr.grunberg.competitionasaservice.competition;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.SequencedCollection;
import java.util.random.RandomGenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.team.Team;

@ExtendWith(MockitoExtension.class)
public class PoolTest {
	private Team team1 = new Team(1, "Team 1", null, Collections.emptyList());
	private Team team2 = new Team(2, "Team 2", null, Collections.emptyList());
	private Team team3 = new Team(3, "Team 3", null, Collections.emptyList());
	
	@Mock
	private RandomGenerator randomGenerator;
	private DataController dataController;
	
	@InjectMocks
	private Game firstGame = new Game(7, new TeamSpot(1, team1, LocalDate.now()), new TeamSpot(2, team2, LocalDate.now()), LocalDate.now().minus(1, ChronoUnit.DAYS));
	@InjectMocks
	private Game secondGame = new Game(4, new TeamSpot(5, team2, LocalDate.now()), new TeamSpot(14, team3, LocalDate.now()), LocalDate.now());
	@InjectMocks
	private Game thirdGame = new Game(3, new TeamSpot(8, team3, LocalDate.now()), new TeamSpot(15, team1, LocalDate.now()), LocalDate.now().plus(1, ChronoUnit.DAYS));
	
	private Pool pool;
	
	@BeforeEach
	public void setUp() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		dataController = new InMemoryDataController(randomGenerator, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
		pool = new Pool(1, "pool", List.of(
				firstGame, secondGame, thirdGame
				));
	}
	
	@Test
	public void getTeamsSpots() {
		assertEquals(3, pool.getTeamSpots().size());
	}
	
	@Test
	public void sortGames() {
		SequencedCollection<Game> sortedGames = pool.getGames().stream().sorted().toList();
		assertEquals(firstGame, sortedGames.getFirst());
		assertEquals(thirdGame, sortedGames.getLast());
	}
	
	@Test
	public void calculateStatisticsTest() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		pool.getGames().forEach(g -> g.play(dataController));
		rigGame(firstGame, 3, 2);
		rigGame(secondGame, 1, 0);
		rigGame(thirdGame, 0, 6);
		pool.recalculateStatistics();
		assertEquals(team1, pool.getTeamAtPosition(1).get());
		assertEquals(team2, pool.getTeamAtPosition(2).get());
		assertEquals(team3, pool.getTeamAtPosition(3).get());
		assertEquals(6, pool.getTeamStatistics(team1).points());
		assertEquals(3, pool.getTeamStatistics(team2).points());
		assertEquals(0, pool.getTeamStatistics(team3).points());
		assertEquals(9, pool.getTeamStatistics(team1).goalsScored());
		assertEquals(2, pool.getTeamStatistics(team1).goalsReceived());
	}
	
	private void rigGame(Game game, int homeGoals, int awayGoals) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Class<?> gameClass = game.getClass();
	    Field homeGoalsField = gameClass.getDeclaredField("homeGoals");
	    Field awayGoalsField = gameClass.getDeclaredField("awayGoals");
	    Field finishedField = gameClass.getDeclaredField("finished");
	    homeGoalsField.setAccessible(true);
	    awayGoalsField.setAccessible(true);
	    finishedField.setAccessible(true);
	    homeGoalsField.set(game, homeGoals);
	    awayGoalsField.set(game, awayGoals);
	    finishedField.set(game, true);
	}
}
