package fr.grunberg.competitionasaservice.competition.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.random.RandomGenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.grunberg.competitionasaservice.competition.Competition;
import fr.grunberg.competitionasaservice.competition.Game;
import fr.grunberg.competitionasaservice.competition.InMemoryDataController;
import fr.grunberg.competitionasaservice.competition.Pool;
import fr.grunberg.competitionasaservice.dao.DataController;
import fr.grunberg.competitionasaservice.geography.Continent;
import fr.grunberg.competitionasaservice.geography.Country;
import fr.grunberg.competitionasaservice.geography.World;
import fr.grunberg.competitionasaservice.team.Team;

@ExtendWith(MockitoExtension.class)
public class CompetitionModelTest {
	@Mock
	private RandomGenerator randomGenerator;
	private DataController dataController;
	private World hyboria;
	private Continent cimmeria;
	private Continent gondwana;
	private Continent laurasia;
	
	@BeforeEach
	public void setUp() {
		hyboria = new World(1, "Age of Hyboria");
		cimmeria = new Continent(2, "Cimmeria", hyboria);
		gondwana = new Continent(3, "Gondwana", hyboria);
		laurasia = new Continent(102, "Laurasia", hyboria);
		Collection<Country> countries = List.of(
				new Country(4, "Amazonia", cimmeria, Collections.emptyList()),
				new Country(5, "Aquilonia", cimmeria, Collections.emptyList()),
				new Country(6, "Zamora", cimmeria, Collections.emptyList()),
				new Country(7, "Kambuja", cimmeria, Collections.emptyList()),
				new Country(8, "Stygia", cimmeria, Collections.emptyList()),
				new Country(9, "Punt", cimmeria, Collections.emptyList()),
				new Country(10, "Ophir", cimmeria, Collections.emptyList()),
				new Country(11, "Turan", cimmeria, Collections.emptyList()),
				new Country(12, "Shem", gondwana, Collections.emptyList()),
				new Country(13, "Vanaheim", gondwana, Collections.emptyList()),
				new Country(16, "Vendhya", gondwana, Collections.emptyList()),
				new Country(17, "Wadai", gondwana, Collections.emptyList()),
				new Country(18, "Wazuli", gondwana, Collections.emptyList()),
				new Country(19, "Yamatai", gondwana, Collections.emptyList()),
				new Country(20, "Zembabwei", gondwana, Collections.emptyList()),
				new Country(21, "Zingara", gondwana, Collections.emptyList()),
				new Country(22, "Asgard", laurasia, Collections.emptyList()),
				new Country(23, "Brythunia", laurasia, Collections.emptyList()),
				new Country(24, "Hyrkania", laurasia, Collections.emptyList()),
				new Country(25, "Khitai", laurasia, Collections.emptyList())
				);
		dataController = new InMemoryDataController(randomGenerator,
				List.of(hyboria), List.of(cimmeria, gondwana), countries, 
				countries.stream().map(c -> new Team(c.id(), c.name(), c, Collections.emptyList())).toList()
				);
	}
	
	@Test
	public void simpleWorldChampionship() {
		CompetitionModel model = new CompetitionModel(1, "Championship",
				List.of(
						new CompetitionStageModel(CompetitionStageType.CHAMPIONSHIP, CompetitionStageGroup.WORLD, hyboria, 0)
						)
				);
		int numberOfTeams = (int) dataController.getTeams().stream().filter(t -> t.country().continent().world().equals(hyboria)).count();
		
		Competition competition = model.instantiate(dataController, LocalDate.of(2020, 2, 29));
		assertNotNull(competition);
		assertEquals("Championship 2020", competition.getName());
		assertEquals(1, competition.getPools().size());
		Pool mainPool = competition.getPools().iterator().next();
		assertEquals(numberOfTeams * (numberOfTeams-1), mainPool.getGames().size());
		assertEquals(numberOfTeams, mainPool.getGames().stream().map(g -> g.getHomeTeam().getTeam().get()).distinct().count());
		assertEquals(numberOfTeams, mainPool.getGames().stream().map(g -> g.getAwayTeam().getTeam().get()).distinct().count());
	}
	
	@Test
	public void simpleContinentalChampionship() {
		CompetitionModel model = new CompetitionModel(2, "Championship",
				List.of(
						new CompetitionStageModel(CompetitionStageType.CHAMPIONSHIP, CompetitionStageGroup.WORLD, cimmeria, 0)
						)
				);
		int numberOfTeams = (int) dataController.getTeams().stream().filter(t -> t.country().continent().equals(cimmeria)).count();
		
		Competition competition = model.instantiate(dataController, LocalDate.of(2020, 2, 29));
		assertNotNull(competition);
		assertEquals("Championship 2020", competition.getName());
		assertEquals(1, competition.getPools().size());
		Pool mainPool = competition.getPools().iterator().next();
		assertEquals(numberOfTeams * (numberOfTeams-1), mainPool.getGames().size());
		assertEquals(numberOfTeams, mainPool.getGames().stream().map(g -> g.getHomeTeam().getTeam().get()).distinct().count());
		assertEquals(numberOfTeams, mainPool.getGames().stream().map(g -> g.getAwayTeam().getTeam().get()).distinct().count());
	}
	
	@Test
	public void simpleElimination() {
		CompetitionModel model = new CompetitionModel(3, "Cup",
				List.of(
						new CompetitionStageModel(CompetitionStageType.ELIMINATION, null, cimmeria, 0)
						)
				);
		
		Competition competition = model.instantiate(dataController, LocalDate.now());
		assertNotNull(competition);
		assertEquals(7, competition.getPools().size());
		Collection<Game> games = competition.getPools().stream()
				.map(p -> p.getGames())
				.flatMap(g -> g.stream())
				.toList();
		assertEquals(7, games.size());
		assertEquals(4, (int) games.stream()
				.filter(g -> g.getHomeTeam().isDecided() && g.getAwayTeam().isDecided())
				.count());
	}
	@Test
	public void multiStageWorldCup() {
		CompetitionModel model = new CompetitionModel(4, "World cup",
				List.of(
						new CompetitionStageModel(CompetitionStageType.POOL, CompetitionStageGroup.CONTINENT, null, 8),
						new CompetitionStageModel(CompetitionStageType.POOL, null, null, 4),
						new CompetitionStageModel(CompetitionStageType.ELIMINATION, CompetitionStageGroup.CONTINENT, null, 0)
						)
				);

		Competition competition = model.instantiate(dataController, LocalDate.now());
		assertNotNull(competition);
		assertEquals(10, competition.getPools().size());
		Collection<Game> games = competition.getPools().stream()
				.map(p -> p.getGames())
				.flatMap(g -> g.stream())
				.toList();
		assertEquals(45, games.size());
		assertEquals(30, (int) games.stream()
				.filter(g -> g.getHomeTeam().isDecided() && g.getAwayTeam().isDecided())
				.count());
	}
	@Test
	public void spreadSelectedSpots_One() {
		Object o = new Object();
		assertEquals(List.of(1, 1, 1), 
				CompetitionModel.spreadSelectedSpots(
					List.of(
							List.of(o, o, o, o, o),
							List.of(o, o, o, o, o),
							List.of(o, o, o, o, o)
							)
					, 3
					)
				);
	}
	@Test
	public void spreadSelectedSpots_Proportional() {
		Object o = new Object();
		assertEquals(List.of(2, 2, 1), 
				CompetitionModel.spreadSelectedSpots(
					List.of(
							List.of(o, o, o, o, o, o),
							List.of(o, o, o, o, o, o),
							List.of(o, o, o)
							)
					, 5
					)
				);
	}
	@Test
	public void spreadSelectedSpots_Uneven() {
		Object o = new Object();
		assertEquals(List.of(2, 2, 1), 
				CompetitionModel.spreadSelectedSpots(
					List.of(
							List.of(o, o, o, o, o, o),
							List.of(o, o, o, o, o, o),
							List.of(o, o, o, o, o)
							)
					, 5
					)
				);
	}
	@Test
	public void spreadSelectedSpots_Dividable() {
		Object o = new Object();
		assertEquals(List.of(2, 2, 2), 
				CompetitionModel.spreadSelectedSpots(
					List.of(
							List.of(o, o, o, o, o, o),
							List.of(o, o, o, o, o, o),
							List.of(o, o, o, o, o)
							)
					, 6
					)
				);
	}
}
