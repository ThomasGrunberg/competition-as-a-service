package fr.grunberg.competitionasaservice.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class MathUtilTest {
	@Test
	public void isPowerOfTwo() {
		assertTrue(MathUtil.isPowerOfTwo(1));
		assertTrue(MathUtil.isPowerOfTwo(2));
		assertFalse(MathUtil.isPowerOfTwo(3));
		assertTrue(MathUtil.isPowerOfTwo(4));
		assertFalse(MathUtil.isPowerOfTwo(5));
		assertFalse(MathUtil.isPowerOfTwo(6));
		assertFalse(MathUtil.isPowerOfTwo(7));
		assertTrue(MathUtil.isPowerOfTwo(8));
		assertFalse(MathUtil.isPowerOfTwo(9));
		assertFalse(MathUtil.isPowerOfTwo(10));
		assertFalse(MathUtil.isPowerOfTwo(11));
		assertFalse(MathUtil.isPowerOfTwo(12));
	}
	@Test
	public void getNearestInferiorOrEqualPowerOfTwo() {
		assertEquals(0, MathUtil.getNearestInferiorOrEqualPowerOfTwo(0));
		assertEquals(1, MathUtil.getNearestInferiorOrEqualPowerOfTwo(1));
		assertEquals(2, MathUtil.getNearestInferiorOrEqualPowerOfTwo(2));
		assertEquals(2, MathUtil.getNearestInferiorOrEqualPowerOfTwo(3));
		assertEquals(4, MathUtil.getNearestInferiorOrEqualPowerOfTwo(4));
		assertEquals(4, MathUtil.getNearestInferiorOrEqualPowerOfTwo(5));
		assertEquals(4, MathUtil.getNearestInferiorOrEqualPowerOfTwo(6));
		assertEquals(4, MathUtil.getNearestInferiorOrEqualPowerOfTwo(7));
		assertEquals(8, MathUtil.getNearestInferiorOrEqualPowerOfTwo(8));
		assertEquals(8, MathUtil.getNearestInferiorOrEqualPowerOfTwo(9));
		assertEquals(8, MathUtil.getNearestInferiorOrEqualPowerOfTwo(10));
		assertEquals(8, MathUtil.getNearestInferiorOrEqualPowerOfTwo(11));
		assertEquals(8, MathUtil.getNearestInferiorOrEqualPowerOfTwo(12));
	}
}